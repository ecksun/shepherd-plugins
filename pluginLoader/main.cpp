#include <QApplication>
 
#include "pluginLoader.h"
 
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    pluginLoader *dialog = new pluginLoader;
 
    dialog->show();
    return app.exec();
}
