HEADERS     = pluginLoader.h 
SOURCES     = pluginLoader.cpp main.cpp
FORMS       = pluginLoader.ui
INCLUDEPATH  += ../shepherd

# install
target.path = pluginLoader
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS *.pro
sources.path = .
INSTALLS += target sources

