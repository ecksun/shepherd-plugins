#include <QtGui>
#include "pluginLoader.h"
#include "../shepherd/interfaces.h"


pluginLoader::pluginLoader() 
{
    setupUi(this);

    QDir pluginsDir;
    QStringList pluginFileNames;

    pluginsDir = QDir(QCoreApplication::instance()->applicationDirPath());
    pluginsDir.cdUp();
    pluginsDir.cd("shepherd");

    foreach (QString plugintype, QStringList() << "triggers" << "actions")
    {
        if (!pluginsDir.cd(plugintype))
        {
            pluginsDir.cdUp();
            pluginsDir.cd(plugintype);
        }

        foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
            if (!fileName.endsWith(".so"))
                continue;

            qDebug() << fileName;
            QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
            QObject *plugin = loader.instance();
            if (plugin) {
    //            populateMenus(plugin);
                pluginFileNames += fileName;
                qDebug() << "Loaded " << plugintype << " (" << qobject_cast<InfoInterface *>(plugin)->name() << ")";
    //            connect(this, SIGNAL(about()), plugin, SLOT(aboutPlugin()));
    //            emit about();
                plugins.insert(qobject_cast<InfoInterface *>(plugin)->name(), qobject_cast<InputInterface *>(plugin));
                // qobject_cast<InputInterface *>(plugin)->configure();
                // dropdown->addItem(fileName);

            } else {
                qWarning() << "FAILED " << plugintype << loader.errorString();
            }
        }
        pluginsDir.cd("..");
    }

    foreach (QString pluginName, plugins.keys()) {
        dropdown->addItem(pluginName);
    }

    connect(pushButton, SIGNAL(clicked()), this, SLOT(load()));
}

void pluginLoader::load()
{
    qDebug() << "Trying to load " << dropdown->currentText();
    plugins[dropdown->currentText()]->configure();
}
