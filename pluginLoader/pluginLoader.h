#ifndef SHEPHERD_PLUGINLOADER_H
#define SHEPHERD_PLUGINLOADER_H
#include "ui_pluginLoader.h"
#include <QtGui>
#include "../shepherd/interfaces.h"

class pluginLoader : public QMainWindow, private Ui::pluginLoader
{
    Q_OBJECT

    public:
        pluginLoader();

    public slots:
        void load();

    private:
        QMap<QString, InputInterface *> plugins;
        
};

#endif
