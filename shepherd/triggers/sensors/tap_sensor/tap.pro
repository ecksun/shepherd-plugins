#! [0]
TEMPLATE      = lib
CONFIG       += plugin
INCLUDEPATH  += ../../..
HEADERS       = tap.h
SOURCES       = tap.cpp
TARGET        = $$qtLibraryTarget(tap)
DESTDIR       = ../../

CONFIG += console mobility
MOBILITY += sensors
