#ifndef SHEPHERD_TAP_SENSOR_PLUGIN_HEADER
#define SHEPHERD_TAP_SENSOR_PLUGIN_HEADER

#include <QTapSensor>
#include <QTapFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief Tap sensor Trigger
 *
 * A Trigger plugin that looks at taps on the phone
 * @note This plugin is untested as there is no output on my device (the n900).
 */
class TapSensorPlugin : public QObject,
                        public QTapFilter,
                        public InputInterface,
                        public InfoInterface,
                        public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        TapSensorPlugin(QObject * parent = 0);
        ~TapSensorPlugin();

        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QTapReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QTapSensor * m_sensor;
        QString tapDirectionDescription(QTapReading::TapDirection);
        bool lastEval;
        QPixmap * pix;

        QTapReading::TapDirection selection;

        QMap<QString, QTapReading::TapDirection> directions;

};

#endif
