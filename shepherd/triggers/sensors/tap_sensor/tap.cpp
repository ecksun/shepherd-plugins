#include "tap.h"
#include <QTapSensor>
#include <QMessageBox>
#include <QInputDialog>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
/**
 * Im not sure this class works as I get no output. However it could be related
 * to the fact that the n900 doesnt have a tap sensor
 */
TapSensorPlugin::TapSensorPlugin(QObject* parent) : 
    QObject(parent), pix(new QPixmap), selection(QTapReading::X)
{
    m_sensor = new QTapSensor(this);
    // This property is not used atm
    m_sensor->setProperty("returnDoubleTapEvents", false);
    m_sensor->addFilter(this);
    m_sensor->start();

    directions.insert("Along the X axis", QTapReading::X);
    directions.insert("Along the Y axis", QTapReading::Y);
    directions.insert("Along the Z axis", QTapReading::Z);
    directions.insert("Towards the positive X direction",  QTapReading::X_Pos);
    directions.insert("Towards the positive Y direction",  QTapReading::Y_Pos);
    directions.insert("Towards the positive Z direction",  QTapReading::Z_Pos);
    directions.insert("Towards the negative X direction",  QTapReading::X_Neg);
    directions.insert("Towards the negative Y direction",  QTapReading::Y_Neg);
    directions.insert("Towards the negative Z direction",  QTapReading::Z_Neg);
}

/**
 * Delete the resources allocated on the heap
 */
TapSensorPlugin::~TapSensorPlugin() {
    delete pix;
    delete m_sensor;
}
 
/**
 * Parse the reading comming from the sensor
 * @see QTapFilter::filter(QTapReading*)
 */
bool TapSensorPlugin::filter(QTapReading* reading)
{
    if (reading->tapDirection() == selection) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
    }
    return false;
}

/**
 * Get the description for a TapDirection.
 * The description are taken directly from the qtm API
 * @see http://doc.qt.nokia.com/qtmobility-1.0/qtapreading.html#TapDirection-enum
 * @param direction The direction of the tap
 * @return A description of the tap direction
 */
QString TapSensorPlugin::tapDirectionDescription(QTapReading::TapDirection direction)
{
    switch (direction)
    {
        case QTapReading::X:
            return "This flag is set if the tap was along the X axis.";
        case QTapReading::Y:
            return "This flag is set if the tap was along the Y axis.";
        case QTapReading::Z:
            return "This flag is set if the tap was along the Z axis.";
        case QTapReading::X_Pos:
            return "This value is returned if the tap was towards the positive X direction.";
        case QTapReading::Y_Pos:
            return "This value is returned if the tap was towards the positive Y direction.";
        case QTapReading::Z_Pos:
            return "This value is returned if the tap was towards the positive Z direction.";
        case QTapReading::X_Neg:
            return "This value is returned if the tap was towards the negative X direction.";
        case QTapReading::Y_Neg:
            return "This value is returned if the tap was towards the negative Y direction.";
        case QTapReading::Z_Neg:
            return "This value is returned if the tap was towards the negative Z direction.";
        default:
            return "Unknown direction";
    }
}
 
/**
 * @see InfoInterface::name()
 */
QString TapSensorPlugin::name()
{
    return QString("Tap sensor trigger");
}

/**
 * @see InfoInterface::description()
 */
QString TapSensorPlugin::description()
{
    QString tmp;
    tmp.append("A trigger plugin that reacts to taps on the phone.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void TapSensorPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* TapSensorPlugin::pixmap()
{
    return pix;
}

/**
 * @see TriggerInterface::eval()
 */
bool TapSensorPlugin::eval()
{
    return lastEval;
}

/**
 * @see TriggerInterface::setConfig(const QVariantMap &)
 */
void TapSensorPlugin::setConfig(const QVariantMap & config) 
{
    if (config.contains("direction")) {
        selection = directions.value(config["direction"].toString());
    }
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & TapSensorPlugin::config()
{
    QVariantMap * config = new QVariantMap();
    config->insert("direction", directions.key(selection));
    return *config;
}

/**
 * @see InputInterface::configure()
 */
bool TapSensorPlugin::configure()
{
    QStringList selections(directions.keys());

    bool ok;
    
    QString respons = QInputDialog::getItem(0, name(), 
            "Enter the tap direction ", selections,
            selections.indexOf(directions.key(selection)), false, &ok);
    // Someone pressed cancel
    if (!ok) {
        return false;
    }

    selection = directions.value(respons);
    return true;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool TapSensorPlugin::configure(QString)
{
    return false;
}
 
Q_EXPORT_PLUGIN2(tap, TapSensorPlugin);
