#include "orientation.h"
#include <QMessageBox>
#include <QOrientationSensor>
#include <QInputDialog>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
/**
 * Start listening on the sensor
 */
OrientationPlugin::OrientationPlugin(QObject* parent) :
    QObject(parent), pix(new QPixmap), selection(QOrientationReading::TopUp)
{
    m_sensor = new QOrientationSensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();


    orientations.insert("Top up", QOrientationReading::TopUp);
    orientations.insert("Top Down", QOrientationReading::TopDown);
    orientations.insert("Left up",  QOrientationReading::LeftUp);
    orientations.insert("Right up", QOrientationReading::RightUp);
    orientations.insert("Face up", QOrientationReading::FaceUp);
    orientations.insert("Face Down", QOrientationReading::FaceDown);
}

/**
 * Delete the resources allocated on the heap
 */
OrientationPlugin::~OrientationPlugin() {
    delete m_sensor;
    delete pix;
}
 
 
/**
 * Parse the reading comming from the sensor
 * @see QOrientationFilter::filter(QOrientationReading*)
 */
bool OrientationPlugin::filter(QOrientationReading* reading)
{
    if (reading->orientation() == selection) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
    }
    return false;
}

/**
 * Get the description for a Orientation.
 * The description are taken directly from the qtm API
 * @see http://doc.qt.nokia.com/qtmobility-1.0/qorientationreading.html#Orientation-enum
 * @param orientation The orientation
 * @return A description of the orientation
 */
QString OrientationPlugin::orientationDescription(QOrientationReading::Orientation orientation)
{
    switch (orientation)
    {
        case QOrientationReading::TopUp:
            return "The Top edge of the device is pointing up.";
        case QOrientationReading::TopDown:
            return "The Top edge of the device is pointing down.";
        case QOrientationReading::LeftUp:
            return "The Left edge of the device is pointing up.";
        case QOrientationReading::RightUp:
            return "The Right edge of the device is pointing up.";
        case QOrientationReading::FaceUp:
            return "The Face of the device is pointing up.";
        case QOrientationReading::FaceDown:
            return "The Face of the device is pointing down.";
        default:
            return "Unknown orientation";
    }
}

/**
 * @see InfoInterface::name()
 */
QString OrientationPlugin::name()
{
    return QString("Orientation sensor plugin");
}

/**
 * @see InfoInterface::description()
 */
QString OrientationPlugin::description()
{
    QString tmp;
    tmp.append("A plugin for reacting to changes on the orientation of the device.\n");
    tmp.append("This plugin might drain the battery faster then normal.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void OrientationPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("A compass plugin by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* OrientationPlugin::pixmap()
{
    return pix;
}

/**
 * @see TriggerInterface::eval()
 */
bool OrientationPlugin::eval()
{
    return lastEval;
}

/**
 * @see TriggerInterface::setConfig(const QVariantMap &)
 */
void OrientationPlugin::setConfig(const QVariantMap & config) 
{
    if (config.contains("orientation")) {
        selection = orientations.value(config["orientation"].toString());
    }
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & OrientationPlugin::config()
{
    QVariantMap * config = new QVariantMap();
    config->insert("orientation", orientations.key(selection));
    return *config;
}

/**
 * @see InputInterface::configure()
 */
bool OrientationPlugin::configure()
{
    QStringList selections(orientations.keys());

    bool ok;
    
    QString respons = QInputDialog::getItem(0, name(), 
            "Enter the light level at which to react", selections,
            selections.indexOf(orientations.key(selection)), false, &ok);
    // Someone pressed cancel
    if (!ok) {
        return false;
    }

    selection = orientations.value(respons);
    return true;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool OrientationPlugin::configure(QString)
{
    return false;
}
 
Q_EXPORT_PLUGIN2(orientation, OrientationPlugin);
