#ifndef SHEPHERD_ORIENTATION_PLUGIN_HEADER
#define SHEPHERD_ORIENTATION_PLUGIN_HEADER

#include <QOrientationSensor>
#include <QOrientationFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief Orientation sensor Trigger
 *
 * A Trigger plugin that looks at the orientation of the device
 */
class OrientationPlugin : public QObject,
                          public QOrientationFilter,
                          public InputInterface,
                          public InfoInterface,
                          public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        OrientationPlugin(QObject * parent = 0);
        ~OrientationPlugin();

        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QOrientationReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QOrientationSensor * m_sensor;
        QString orientationDescription(QOrientationReading::Orientation);
        bool lastEval;
        QPixmap * pix;

        QOrientationReading::Orientation selection;

        QMap<QString, QOrientationReading::Orientation> orientations;

};

#endif
