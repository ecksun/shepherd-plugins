#ifndef SHEPHERD_PROXIMITY_SENSOR_PLUGIN_HEADER
#define SHEPHERD_PROXIMITY_SENSOR_PLUGIN_HEADER

#include <QProximitySensor>
#include <QProximityFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief Proximity sensor Trigger
 *
 * A trigger plugin that uses the proximity sensor to see if something is close
 * to the phone or not
 */
class ProximitySensorPlugin : public QObject,
                              public QProximityFilter,
                              public InputInterface,
                              public InfoInterface,
                              public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        ProximitySensorPlugin(QObject * parent = 0);
        ~ProximitySensorPlugin();

        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QProximityReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QProximitySensor * m_sensor;
        QPixmap * pix;
        bool somethingCloseLastEval;
};

#endif
