#include "proximity.h"
#include <QProximitySensor>
#include <QDebug>
#include <QMessageBox>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * Set up the sensor
 */
ProximitySensorPlugin::ProximitySensorPlugin(QObject* parent) : QObject(parent), pix(new QPixmap)
{
    m_sensor = new QProximitySensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

/**
 * Delete the resources allocated on the heap
 */
ProximitySensorPlugin::~ProximitySensorPlugin() {
    delete m_sensor;
    delete pix;
}
 
 

/**
 * Parse the reading comming from the sensor
 * @see QProximityFilter::filter(QProximityReading*)
 */
bool ProximitySensorPlugin::filter(QProximityReading* reading)
{
    // Check if both are not the same, in which case the state has changed
    if (!(somethingCloseLastEval && reading->close())) {
        somethingCloseLastEval = !somethingCloseLastEval;
        emit stateChanged(somethingCloseLastEval);
        if (somethingCloseLastEval) {
            emit turnedTrue();
        }
        else {
            emit turnedFalse();
        }
    }
    return false;
}
 
/**
 * @see InfoInterface::name()
 */
QString ProximitySensorPlugin::name()
{
    return QString("Proximity Sensor trigger");
}

/**
 * @see InfoInterface::description()
 */
QString ProximitySensorPlugin::description()
{
    QString tmp;
    tmp.append("A plugin that watches if something gets close to the phone ");
    tmp.append("by use of the proximity sensor.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void ProximitySensorPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n Note that this plugin might use alot of battery as a sensor is used");
    desc.append("\nby Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* ProximitySensorPlugin::pixmap()
{
    return pix;
}

/**
 * @see TriggerInterface::eval()
 */
bool ProximitySensorPlugin::eval()
{
    return m_sensor->reading()->close();
}

/**
 * There is nothing to configure
 * @see TriggerInterface::setConfig(const QVariantMap &)
 */
void ProximitySensorPlugin::setConfig(const QVariantMap &) 
{
}

/**
 * There is nothing to configure
 * @see InputInterface::config()
 */
const QVariantMap & ProximitySensorPlugin::config()
{
    QVariantMap * config = new QVariantMap();
    return *config;
}

/**
 * There is nothing to configure
 * @see InputInterface::configure()
 */
bool ProximitySensorPlugin::configure()
{
    QString description = "This plugin has nothing to configure as the only ";
    description.append("capability is to sens if something is close.");
    QMessageBox::information(0, name(), description);
    return true;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool ProximitySensorPlugin::configure(QString)
{
    return false;
}
 
Q_EXPORT_PLUGIN2(proximity, ProximitySensorPlugin);
