#include "magnetometer.h"
#include "../XYZInput/XYZInputDialog.h"
#include <QMagnetometer>
#include <QDebug>
#include <QMessageBox>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
/*
 * TODO: Note that this plugin is more or less a c/p of the accelerometer
 * plugin, perhaps they can be merged to minimize code duplication?
 */
 
/**
 * This class is untested as there is no output on the N900 (probably because
 * there is no magnetometer in that device)
 */
MagnetometerPlugin::MagnetometerPlugin(QObject* parent) : 
    QObject(parent), pix(new QPixmap), lastEval(false),
    xConfigValue(0), yConfigValue(0), zConfigValue(0),
    xGreaterThen(true), yGreaterThen(true), zGreaterThen(true)
{
    m_sensor = new QMagnetometer(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

/**
 * Delete all allocated resources on the heap
 */
MagnetometerPlugin::~MagnetometerPlugin() {
    delete m_sensor;
    delete pix;
}
 
 
/**
 * Get a reading from the magnetometer and parses it
 * @see QMagnetometerFilter::filter(QMagnetometerReading *)
 */
bool MagnetometerPlugin::filter(QMagnetometerReading* reading)
{
    if (   ((xGreaterThen && reading->x() > xConfigValue) || (!xGreaterThen && reading->x() < xConfigValue))
        && ((yGreaterThen && reading->y() > yConfigValue) || (!yGreaterThen && reading->y() < yConfigValue))
        && ((zGreaterThen && reading->z() > zConfigValue) || (!zGreaterThen && reading->z() < zConfigValue))
       ) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else {
        if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
        }
    }
    return false;
}
 
/**
 * @see TriggerInterface::eval()
 */
bool MagnetometerPlugin::eval()
{
    return lastEval;
}

/**
 * @see InfoInterface::name()
 */
QString MagnetometerPlugin::name()
{
    return QString("Magnetometer plugin");
}

/**
 * @see InfoInterface::description()
 */
QString MagnetometerPlugin::description()
{
    QString tmp;
    tmp.append("A plugin for reacting to magnetometer changes.\n");
    tmp.append("Note that this plugin probably consumes a lot of battery.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void MagnetometerPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n");
    desc.append("A magnetometer plugin by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* MagnetometerPlugin::pixmap()
{
    return pix;

}
/**
 * @see InputInterface::configure()
 */
bool MagnetometerPlugin::configure()
{
    XYZInputDialog input(xConfigValue, xGreaterThen, yConfigValue, yGreaterThen, 
            zConfigValue, zGreaterThen);
    if (!input.exec()) {
        return false;
    }

    xConfigValue = input.getX();
    yConfigValue = input.getY();
    zConfigValue = input.getZ();

    xGreaterThen = input.getXRelation();
    yGreaterThen = input.getYRelation();
    zGreaterThen = input.getZRelation();

    return true;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool MagnetometerPlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & MagnetometerPlugin::config()
{
    QVariantMap * config = new QVariantMap;
    config->insert("xConfigValue", xConfigValue);
    config->insert("yConfigValue", yConfigValue);
    config->insert("zConfigValue", zConfigValue);
    config->insert("xGreaterThen", xGreaterThen);
    config->insert("yGreaterThen", yGreaterThen);
    config->insert("zGreaterThen", zGreaterThen);
    return *config;
}

/**
 * @see TriggerInterface::setConfig()
 */
void MagnetometerPlugin::setConfig(const QVariantMap &config) 
{
    if (    config.contains("xConfigValue") && 
            config.contains("yConfigValue") && 
            config.contains("zConfigValue") && 
            config.contains("xGreaterThen") && 
            config.contains("yGreaterThen") && 
            config.contains("zGreaterThen"))
    {
        xConfigValue = config["xConfigValue"].toInt();
        yConfigValue = config["yConfigValue"].toInt();
        zConfigValue = config["zConfigValue"].toInt();

        xGreaterThen = config["xGreaterThen"].toBool();
        yGreaterThen = config["yGreaterThen"].toBool();
        zGreaterThen = config["zGreaterThen"].toBool();
    }
}
Q_EXPORT_PLUGIN2(magnetometer, MagnetometerPlugin);
