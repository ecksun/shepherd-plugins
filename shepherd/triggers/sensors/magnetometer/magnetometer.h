#ifndef SHEPHERD_COMPASS_PLUGIN_HEADER
#define SHEPHERD_COMPASS_PLUGIN_HEADER

#include <QMagnetometer>
#include <QMagnetometerFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief Magnetometer Trigger
 *
 * A trigger plugin that uses the magnetometer.
 * @note This plugin is untested as I get no output on my device (N900). Probably because there are no magnetometer on it
 */
class MagnetometerPlugin : public QObject,
                           public QMagnetometerFilter,
                           public InputInterface,
                           public InfoInterface,
                           public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        MagnetometerPlugin(QObject * parent = 0);
        ~MagnetometerPlugin();

        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QMagnetometerReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QMagnetometer * m_sensor;
        QPixmap * pix;
        bool lastEval;

        int xConfigValue;
        int yConfigValue;
        int zConfigValue;

        bool xGreaterThen;
        bool yGreaterThen;
        bool zGreaterThen;
};

#endif
