#ifndef SHEPHERD_ROTATION_SENSOR_PLUGIN_HEADER
#define SHEPHERD_ROTATION_SENSOR_PLUGIN_HEADER

#include <QRotationSensor>
#include <QRotationFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief Rotation sensor Trigger
 *
 * A Trigger plugin that looks for rotations of the device
 */
class RotationSensorPlugin : public QObject,
                             public QRotationFilter,
                             public InputInterface,
                             public InfoInterface,
                             public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        RotationSensorPlugin(QObject * parent = 0);
        ~RotationSensorPlugin();
        
        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QRotationReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QRotationSensor * m_sensor;
        QPixmap * pix;
        bool lastEval;

        int xConfigValue;
        int yConfigValue;
        int zConfigValue;

        bool xGreaterThen;
        bool yGreaterThen;
        bool zGreaterThen;
};

#endif
