#include "rotation.h"
#include "../XYZInput/XYZInputDialog.h"
#include <QRotationSensor>
#include <QDebug>
#include <QMessageBox>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
/*
 * TODO: Note that this plugin is more or less a c/p of the accelerometer
 * plugin, perhaps they can be merged to minimize code duplication?
 */
 
/**
 * Initialize the sensor
 */
RotationSensorPlugin::RotationSensorPlugin(QObject* parent) : 
    QObject(parent), pix(new QPixmap), lastEval(false),
    xConfigValue(0), yConfigValue(0), zConfigValue(0),
    xGreaterThen(true), yGreaterThen(true), zGreaterThen(true)
{
    m_sensor = new QRotationSensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

/**
 * Delete all resources allocated on the heap
 */
RotationSensorPlugin::~RotationSensorPlugin() {
    delete m_sensor;
    delete pix;
}
 
 
/**
 * Recive and parse a reading from the sensor
 * @see QRotationFilter::filter(QRotationReading *)
 */
bool RotationSensorPlugin::filter(QRotationReading* reading)
{
    if (   ((xGreaterThen && reading->x() > xConfigValue) || (!xGreaterThen && reading->x() < xConfigValue))
        && ((yGreaterThen && reading->y() > yConfigValue) || (!yGreaterThen && reading->y() < yConfigValue))
        && ((zGreaterThen && reading->z() > zConfigValue) || (!zGreaterThen && reading->z() < zConfigValue))
       ) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else {
        if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
        }
    }
    return false;
}

/**
 * @see TriggerInterface::eval()
 */
bool RotationSensorPlugin::eval()
{
    return lastEval;
}

/**
 * @see InfoInterface::name()
 */
QString RotationSensorPlugin::name()
{
    return QString("Rotation sensor trigger");
}

/**
 * @see InfoInterface::description()
 */
QString RotationSensorPlugin::description()
{
    QString tmp;
    tmp.append("This plugin watches for rotation changes. \n");
    tmp.append("Note that this plugin probably consumes a lot of battery.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void RotationSensorPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n");
    desc.append("This plugin is written by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* RotationSensorPlugin::pixmap()
{
    return pix;

}
/**
 * @see InputInterface::configure()
 */
bool RotationSensorPlugin::configure()
{
    XYZInputDialog input(xConfigValue, xGreaterThen, yConfigValue, yGreaterThen, 
            zConfigValue, zGreaterThen);
    if (!input.exec()) {
        return false;
    }

    xConfigValue = input.getX();
    yConfigValue = input.getY();
    zConfigValue = input.getZ();

    xGreaterThen = input.getXRelation();
    yGreaterThen = input.getYRelation();
    zGreaterThen = input.getZRelation();

    return true;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool RotationSensorPlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & RotationSensorPlugin::config()
{
    QVariantMap * config = new QVariantMap;
    config->insert("xConfigValue", xConfigValue);
    config->insert("yConfigValue", yConfigValue);
    config->insert("zConfigValue", zConfigValue);
    config->insert("xGreaterThen", xGreaterThen);
    config->insert("yGreaterThen", yGreaterThen);
    config->insert("zGreaterThen", zGreaterThen);
    return *config;
}

/**
 * @see TriggerInterface::setConfig()
 */
void RotationSensorPlugin::setConfig(const QVariantMap &config) 
{
    if (    config.contains("xConfigValue") && 
            config.contains("yConfigValue") && 
            config.contains("zConfigValue") && 
            config.contains("xGreaterThen") && 
            config.contains("yGreaterThen") && 
            config.contains("zGreaterThen"))
    {
        xConfigValue = config["xConfigValue"].toInt();
        yConfigValue = config["yConfigValue"].toInt();
        zConfigValue = config["zConfigValue"].toInt();

        xGreaterThen = config["xGreaterThen"].toBool();
        yGreaterThen = config["yGreaterThen"].toBool();
        zGreaterThen = config["zGreaterThen"].toBool();
    }
}
Q_EXPORT_PLUGIN2(rotation, RotationSensorPlugin);
