#include "ui_XYZInput.h"
#include "XYZInputDialog.h"
#include <QIntValidator>

/**
 * Setup the UI, set the default checkboxes/texts and connect the slots with
 * the appropriate signals
 */
XYZInputDialog::XYZInputDialog(
        int xInit, bool xGreater, 
        int yInit, bool yGreater, 
        int zInit, bool zGreater, 
        int min, int max, 
        QWidget *parent) :
    QDialog(parent), 
    X(xInit), Y(yInit), Z(zInit), 
    xGreaterThen(xGreater), yGreaterThen(yGreater), zGreaterThen(zGreater),
    minValue(min), maxValue(max)
{
    setupUi(this);

    validator = new QIntValidator(minValue, maxValue, this);    
    XValue->setValidator(validator);
    YValue->setValidator(validator);
    ZValue->setValidator(validator);
    
    XSlider->setMinimum(minValue);
    XSlider->setMaximum(maxValue);
    XSlider->setValue(X);
    XUpdated(X);
    if (!xGreaterThen) {
        XRelationsButton->setText("<");
    }

    YSlider->setMinimum(minValue);
    YSlider->setMaximum(maxValue);
    YSlider->setValue(Y);
    YUpdated(Y);
    if (!yGreaterThen) {
        YRelationsButton->setText("<");
    }

    ZSlider->setMinimum(minValue);
    ZSlider->setMaximum(maxValue);
    ZSlider->setValue(Z);
    ZUpdated(Z);
    if (!zGreaterThen) {
        ZRelationsButton->setText("<");
    }

    connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
    connect(XRelationsButton, SIGNAL(clicked()), this, SLOT(XRelationChange()));
    connect(YRelationsButton, SIGNAL(clicked()), this, SLOT(YRelationChange()));
    connect(ZRelationsButton, SIGNAL(clicked()), this, SLOT(ZRelationChange()));
    
    connect(XSlider, SIGNAL(valueChanged(int)), this, SLOT(XUpdated(int)));
    connect(YSlider, SIGNAL(valueChanged(int)), this, SLOT(YUpdated(int)));
    connect(ZSlider, SIGNAL(valueChanged(int)), this, SLOT(ZUpdated(int)));

    connect(XValue, SIGNAL(textEdited(QString)), this, SLOT(XUpdated(QString)));
    connect(YValue, SIGNAL(textEdited(QString)), this, SLOT(YUpdated(QString)));
    connect(ZValue, SIGNAL(textEdited(QString)), this, SLOT(ZUpdated(QString)));
}

/**
 * Delete all resources allocated on the heap
 */
XYZInputDialog::~XYZInputDialog()
{
    delete validator;
}

/**
 * A slot that is called when the XSlider is updated
 *
 * @param value The new dialog value
 */
void XYZInputDialog::XUpdated(int value)
{
    XValue->setText(QString::number(value));
}

/**
 * A slot that is called when the YSlider is updated
 *
 * @param value The new dialog value
 */
void XYZInputDialog::YUpdated(int value)
{
    YValue->setText(QString::number(value));
}

/**
 * A slot that is called when the ZSlider is updated
 *
 * @param value The new dialog value
 */
void XYZInputDialog::ZUpdated(int value)
{
    ZValue->setText(QString::number(value));
}


/**
 * A slot that is called when the X input dialog is updated
 *
 * @param value The new dialog value
 */
void XYZInputDialog::XUpdated(QString value)
{
    XSlider->setValue(value.toInt());
}

/**
 * A slot that is called when the Y input dialog is updated
 *
 * @param value The new dialog value
 */
void XYZInputDialog::YUpdated(QString value)
{
    YSlider->setValue(value.toInt());
}

/**
 * A slot that is called when the Z input dialog is updated
 *
 * @param value The new dialog value
 */
void XYZInputDialog::ZUpdated(QString value)
{
    ZSlider->setValue(value.toInt());
}

/**
 * A slot that is called when the X relations button is pressed
 */
void XYZInputDialog::XRelationChange()
{
    xGreaterThen = !xGreaterThen;
    if (xGreaterThen) {
        XRelationsButton->setText(">");
    }
    else {
        XRelationsButton->setText("<");
    }
}

/**
 * A slot that is called when the Y relations button is pressed
 */
void XYZInputDialog::YRelationChange()
{
    yGreaterThen = !yGreaterThen;
    if (yGreaterThen) {
        YRelationsButton->setText(">");
    }
    else {
        YRelationsButton->setText("<");
    }
}

/**
 * A slot that is called when the Z relations button is pressed
 */
void XYZInputDialog::ZRelationChange()
{
    zGreaterThen = !zGreaterThen;
    if (zGreaterThen) {
        ZRelationsButton->setText(">");
    }
    else {
        ZRelationsButton->setText("<");
    }
}

/**
 * Getter function for the X value
 * 
 * @return The Z value
 */
int XYZInputDialog::getX()
{
    return X;
}

/**
 * Getter function for the Y value
 * 
 * @return The Z value
 */
int XYZInputDialog::getY()
{
    return Y;
}

/**
 * Getter function for the Z value
 * 
 * @return The Z value
 */
int XYZInputDialog::getZ()
{
    return Z;
}

/**
 * Getter function for the X relation.
 *
 * @return True if the user inputed value should be larger then the sensor
 * value
 */
bool XYZInputDialog::getXRelation()
{
    return xGreaterThen;
}

/**
 * Getter function for the Y relation.
 *
 * @return True if the user inputed value should be larger then the sensor
 * value
 */
bool XYZInputDialog::getYRelation()
{
    return yGreaterThen;
}

/**
 * Getter function for the Z relation.
 *
 * @return True if the user inputed value should be larger then the sensor
 * value
 */
bool XYZInputDialog::getZRelation()
{
    return zGreaterThen;
}

/**
 * The slot that is called when the save button is pushed
 * This method saves the current state and closes the dialog
 */
void XYZInputDialog::save()
{
    X = XSlider->value();
    Y = YSlider->value();
    Z = ZSlider->value();
    this->accept();
}
