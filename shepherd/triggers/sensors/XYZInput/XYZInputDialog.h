#ifndef SHEPHERD_XYZ_INPUT_DIALOG_H
#define SHEPHERD_XYZ_INPUT_DIALOG_H

#include <QtGui/QDialog>
#include "ui_XYZInput.h"

/**
 * \brief A Input dialog for 3D coordinate limits.
 *
 * A input dialog for asking the user for three diffrent values. The thought is
 * for it to be used when the developer wants values for the three coordinate
 * axis in 3D space. The user can input three values (using a slider or normal
 * numbers) aswell as define their releationship, meaning that the user can
 * select if the sensor value should be either bigger or smaller then the
 * configuration value.
 */
class XYZInputDialog : public QDialog, public Ui::XYZInputDialog
{
    Q_OBJECT
    public:
        explicit XYZInputDialog(int, bool, int, bool, int, bool, int min = 0, int max = 100, QWidget * = 0);
        ~XYZInputDialog();
        int getX();
        int getY();
        int getZ();

        bool getXRelation();
        bool getYRelation();
        bool getZRelation();
        
    public slots:
        void save();

    private slots:
        void XUpdated(int);
        void YUpdated(int);
        void ZUpdated(int);
        void XUpdated(QString);
        void YUpdated(QString);
        void ZUpdated(QString);
        void XRelationChange();
        void YRelationChange();
        void ZRelationChange();


    private:
        int X;
        int Y; 
        int Z;

        bool xGreaterThen;
        bool yGreaterThen;
        bool zGreaterThen;

        int minValue;
        int maxValue;

        QIntValidator * validator;
};

#endif

