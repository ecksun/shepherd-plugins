#ifndef SHEPHERD_RANGE_INPUT_DIALOG_H
#define SHEPHERD_RANGE_INPUT_DIALOG_H

#include <QtGui/QDialog>
#include "ui_rangeInput.h"

/**
 * \brief A input dialog for inputing a range
 *
 * A input dialog that asks the user for inputing a range.
 */
class RangeInputDialog : public QDialog, public Ui::RangeInputDialog
{
    Q_OBJECT
    public:
        explicit RangeInputDialog(int, bool, int, bool, int min = 0, int max = 100, QWidget * = 0);
        ~RangeInputDialog();
        int getMin();
        int getMax();

        void setTitle(QString);
        void setDescription(QString);
        
        bool getMinRelation();
        bool getMaxRelation();
        
    public slots:
        void save();

    private slots:
        void minUpdated(int);
        void maxUpdated(int);
        void minUpdated(QString);
        void maxUpdated(QString);
        void minRelationChange();
        void maxRelationChange();


    private:
        int minValue;
        int maxValue;

        bool minRelation;
        bool maxRelation;

        int min;
        int max;

        QIntValidator * validator;
};

#endif

