#include "ui_rangeInput.h"
#include "rangeInputDialog.h"
#include <QIntValidator>

/**
 * Setup the UI, set the default checkboxes/texts and connect the slots with
 * the appropriate signals
 */
RangeInputDialog::RangeInputDialog(
        int minValueInit, bool minRelationInit,
        int maxValueInit, bool maxRelationInit, 
        int minInit, int maxInit,
        QWidget *parent) :
    QDialog(parent), 
    minValue(minValueInit), maxValue(maxValueInit), 
    minRelation(minRelationInit), maxRelation(maxRelationInit),
    min(minInit), max(maxInit)
{
    setupUi(this);

    validator = new QIntValidator(min, max, this);    
    minValueEdit->setValidator(validator);
    maxValueEdit->setValidator(validator);
    
    minSlider->setMinimum(min);
    minSlider->setMaximum(max);
    minSlider->setValue(minValue);
    minUpdated(minValue);
    if (!minRelation) {
        minRelationButton->setText("<");
    }

    maxSlider->setMinimum(min);
    maxSlider->setMaximum(max);
    maxSlider->setValue(maxValue);
    maxUpdated(maxValue); 
    if (!maxRelation) {
        maxRelationButton->setText("<");
    }

    connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));

    connect(minRelationButton, SIGNAL(clicked()), this, SLOT(minRelationChange()));
    connect(maxRelationButton, SIGNAL(clicked()), this, SLOT(maxRelationChange()));
    
    connect(minSlider, SIGNAL(valueChanged(int)), this, SLOT(minUpdated(int)));
    connect(maxSlider, SIGNAL(valueChanged(int)), this, SLOT(maxUpdated(int)));

    connect(minValueEdit, SIGNAL(textEdited(QString)), this, SLOT(minUpdated(QString)));
    connect(maxValueEdit, SIGNAL(textEdited(QString)), this, SLOT(maxUpdated(QString)));
}

/**
 * Destroy objects created on the heap
 */
RangeInputDialog::~RangeInputDialog()
{
    delete validator;
}

/**
 * Set the title on the dialog
 * @param title the title to be displayed
 */
void RangeInputDialog::setTitle(QString title)
{
    setWindowTitle(title);
}

/**
 * Display a description to the dialog
 * @param desc The description that should be displayed
 */
void RangeInputDialog::setDescription(QString desc)
{
    description->setText(desc);
}

/**
 * A slot for updating the text in the QTextEdit minValueEdit field
 * @param value The value to be set
 */
void RangeInputDialog::minUpdated(int value)
{
    minValueEdit->setText(QString::number(value));
}

/**
 * A slot for updating the text in the QTextEdit maxValueEdit field
 * @param value The value to be set
 */
void RangeInputDialog::maxUpdated(int value)
{
    maxValueEdit->setText(QString::number(value));
}

/**
 * A slot for updating the value of the minSlider
 * @param value The value to be set
 */
void RangeInputDialog::minUpdated(QString value)
{
    minSlider->setValue(value.toInt());
}

/**
 * A slot for updating the value of the maxSlider
 * @param value The value to be set
 */
void RangeInputDialog::maxUpdated(QString value)
{
    maxSlider->setValue(value.toInt());
}

/**
 * A slot that is called every time the minRelationButton is pressed.
 * When the minRelation is true, the user-inserted value should be greater then
 * the sensor value. And vice versa.
 * If minRelation equals true; X > sensor.value();
 */ 
void RangeInputDialog::minRelationChange()
{
    minRelation = !minRelation;
    if (minRelation) {
        minRelationButton->setText(">");
    }
    else {
        minRelationButton->setText("<");
    }
}

/**
 * A slot that is called every time the maxRelationButton is pressed.
 * When the maxRelation is true, the user-inserted value should be greater then
 * the sensor value. And vice versa.
 * If maxRelation equals true; X > sensor.value();
 */ 
void RangeInputDialog::maxRelationChange()
{
    maxRelation  = !maxRelation;
    if (maxRelation) {
        maxRelationButton->setText(">");
    }
    else {
        maxRelationButton->setText("<");
    }
}

/**
 * Getter method for the min value the user has inserted.
 * @return the minimum value the user inserted
 */
int RangeInputDialog::getMin()
{
    return minValue;
}

/**
 * Getter method for the max value the user has inserted.
 * @return the maximum value the user inserted
 */
int RangeInputDialog::getMax()
{
    return maxValue;
}

/**
 * A getter method for getting the relation between the user inserted minimum
 * value and the sensor value.
 * @return true if the user whishes the minimum value should be greater then
 * the sensor value.
 */
bool RangeInputDialog::getMinRelation()
{
    return minRelation;
}

/**
 * A getter method for getting the relation between the user inserted maximum
 * value and the sensor value.
 * @return true if the user whishes the maximum value should be greater then
 * the sensor value.
 */
bool RangeInputDialog::getMaxRelation()
{
    return maxRelation;
}

/**
 * The slot that is called when the save button is pushed
 * This method saves the current state and closes the dialog
 */
void RangeInputDialog::save()
{
    minValue = minSlider->value();
    maxValue = maxSlider->value();
    this->accept();
}

