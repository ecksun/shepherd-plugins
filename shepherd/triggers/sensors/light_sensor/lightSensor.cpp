#include "lightSensor.h"
#include "../rangeInputDialog/rangeInputDialog.h"
#include <QAmbientLightSensor>
#include <QDebug>
#include <QMessageBox>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * Initialize the sensor
 */
LightSensorPlugin::LightSensorPlugin(QObject* parent) : 
    QObject(parent), pix(new QPixmap),
    minValue(0), maxValue(6), minRelation(false), maxRelation(false)
{
    m_sensor = new QAmbientLightSensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

LightSensorPlugin::~LightSensorPlugin() {
    delete m_sensor;
    delete pix;
}

/**
 * Parse the reading comming from the sensor
 * @see QAmbientLightFilter::filter(QAmbientLightReading*)
 */
bool LightSensorPlugin::filter(QAmbientLightReading* reading)
{
    // Check if the min value check out
    if (    (   (minRelation && minValue > reading->lightLevel()) 
            ||  (!minRelation && minValue < reading->lightLevel())) 
        // Check out if the max value checks out
        &&  (   (maxRelation && maxValue > reading->lightLevel()) 
            ||  (!maxRelation && maxValue < reading->lightLevel()))) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else {
        if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
        }
    }
    return false;
}

/**
 * Get the description for a LightLevel.
 * The description are taken directly from the qtm API
 * @see http://doc.qt.nokia.com/qtmobility-1.0/qambientlightreading.html#LightLevel-enum
 * @param level The LightLevel to which a description should be returned
 * @return A description of the lightlevel
 */
QString LightSensorPlugin::lightDescription(QAmbientLightReading::LightLevel level)
{
    switch (level)
    {
        case QAmbientLightReading::Dark: 
            return "It is dark.";
        case QAmbientLightReading::Twilight:
            return "It is moderately dark.";
        case QAmbientLightReading::Light:
            return "It is light (eg. internal lights).";
        case QAmbientLightReading::Bright:
            return "It is bright (eg. shade).";
        case QAmbientLightReading::Sunny:
            return "It is very bright (eg. direct sunlight).";
        default:
            return "Unknown level";
    }
}


/**
 * @see InfoInterface::name()
 */
QString LightSensorPlugin::name()
{
    return QString("Ambient Light Sensor plugin");
}

/**
 * @see InfoInterface::description()
 */
QString LightSensorPlugin::description()
{
    QString tmp;
    tmp.append("A plugin for reacting to ambient light changes.\n");
    tmp.append("This plugin uses the ambient light sensor.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void LightSensorPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n");
    desc.append("If the dynamical background light is enabled in settings on ");
    desc.append("your phone this sensor should be enabled anyway, which should ");
    desc.append("mean that there are no large battery performance impact.\n");
    desc.append("Note however that the above statement is untested");
    desc.append("A compass plugin by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* LightSensorPlugin::pixmap()
{
    return pix;

}

/**
 * @see TriggerInterface::eval()
 */
bool LightSensorPlugin::eval()
{
    return lastEval;
}

/**
 * @see TriggerInterface::setConfig(const QVariantMap &)
 */
void LightSensorPlugin::setConfig(const QVariantMap & config) 
{
    if (   config.contains("minValue") && config.contains("maxValue")
        && config.contains("minRelation") && config.contains("maxRelation"))
    {
        minValue = config["minValue"].toInt();
        maxValue = config["maxValue"].toInt();
        minRelation = config["minRelation"].toBool();
        maxRelation = config["maxRelation"].toBool();
    }
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & LightSensorPlugin::config()
{
    QVariantMap * config = new QVariantMap();
    config->insert("minValue", minValue);
    config->insert("maxValue", maxValue);
    config->insert("minRelation", minRelation);
    config->insert("maxRelation", maxRelation);
    return *config;
}

/**
 * @see InputInterface::configure()
 */
bool LightSensorPlugin::configure()
{
    RangeInputDialog input(minValue, minRelation, maxValue, maxRelation, 0, 6);

    QString description;
    for (int i = 1; i <= 5; ++i) {
        description.append(QString::number(i));
        description.append(": ");
        description.append(lightDescription((QAmbientLightReading::LightLevel)i));
        description.append("\n");
    }
    input.setTitle(name());
    input.setDescription(description);

    if (!input.exec()) {
        return false;
    }

    minValue = input.getMin();
    maxValue = input.getMax();
    minRelation = input.getMinRelation();
    maxRelation = input.getMaxRelation();
    return false;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool LightSensorPlugin::configure(QString)
{
    return false;
}
 
Q_EXPORT_PLUGIN2(lightSensor, LightSensorPlugin);
