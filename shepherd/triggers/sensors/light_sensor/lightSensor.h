#ifndef SHEPHERD_AMBIENT_LIGHT_SENSOR_PLUGIN_HEADER
#define SHEPHERD_AMBIENT_LIGHT_SENSOR_PLUGIN_HEADER

#include <QAmbientLightSensor>
#include <QAmbientLightFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief Ambient light sensor plugin.
 *
 * A trigger plugin that uses the ambient light sensor to look for changes in
 * the ambient light by the phone.
 */
class LightSensorPlugin : public QObject,
                          public QAmbientLightFilter,
                          public InputInterface,
                          public InfoInterface,
                          public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)
        
    public:
        LightSensorPlugin(QObject * parent = 0);
        ~LightSensorPlugin();
        
        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QAmbientLightReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QAmbientLightSensor * m_sensor;
        QString lightDescription(QAmbientLightReading::LightLevel);
        bool lastEval;
        QPixmap * pix;

        int minValue;
        int maxValue;
        bool minRelation;
        bool maxRelation;
};

#endif
