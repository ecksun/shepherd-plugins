#ifndef SHEPHERD_COMPASS_PLUGIN_HEADER
#define SHEPHERD_COMPASS_PLUGIN_HEADER

#include <QCompass>
#include <QCompassFilter>

#include "interfaces.h"

QTM_USE_NAMESPACE

/** 
 * \brief A trigger that uses the compass.
 *
 * A trigger plugin that uses the compass to figure out in what directino the phone is heading
 *
 * @note This plugin is untested as I have no device with a compass.
 * @note The N900 have no compass
 */
class CompassPlugin : public QObject,
                      public QCompassFilter,
                      public InputInterface,
                      public InfoInterface,
                      public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)
    public:
        CompassPlugin(int min = 0, bool minRelation = false, 
                int max = 0, bool maxRelation = false, QObject * parent = 0);
        ~CompassPlugin();

        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QCompassReading *);
    
    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QCompass * m_sensor;
        bool lastEval;
        QPixmap * pix;

        int minValue;
        int maxValue;
        int minRelation;
        int maxRelation;
};

#endif
