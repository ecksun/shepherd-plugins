#include "compass.h"
#include <QCompass>
#include <QDebug>
#include <QMessageBox>
#include "../rangeInputDialog/rangeInputDialog.h"
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * This class is untested as there is no output on the N900 (probably because
 * there is no compass in that device
 */
CompassPlugin::CompassPlugin(int minValueInit, bool minRelationInit, 
        int maxValueInit, bool maxRelationInit, QObject* parent) : 
    QObject(parent), lastEval(false), pix(new QPixmap),
    minValue(minValueInit),maxValue(maxValueInit), 
    minRelation(minRelationInit), maxRelation(maxRelationInit)
{
    m_sensor = new QCompass(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

CompassPlugin::~CompassPlugin() {
    delete m_sensor;
    delete pix;
}
 
/**
 * @see InfoInterface::name()
 */
QString CompassPlugin::name()
{
    return QString("Compass plugin");
}

/**
 * @see InfoInterface::description()
 */
QString CompassPlugin::description()
{
    QString tmp;
    tmp.append("A plugin for reacting to compass changes.\n");
    tmp.append("Note that this plugin is untested and might use alot of battery.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void CompassPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n");
    desc.append("A compass plugin by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* CompassPlugin::pixmap()
{
    return pix;

}

/**
 * Parse the reading comming from the sensor
 * @see QCompassFilter::filter(QCompassReading*)
 */
bool CompassPlugin::filter(QCompassReading* reading)
{
    // Check if the min value check out
    if (    (   (minRelation && minValue > reading->azimuth()) 
            ||  (!minRelation && minValue < reading->azimuth())) 
        // Check out if the max value checks out
        &&  (   (maxRelation && maxValue > reading->azimuth()) 
            ||  (!maxRelation && maxValue < reading->azimuth()))) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else {
        if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
        }
    }
    return false;
}
 
/**
 * @see TriggerInterface::eval()
 */
bool CompassPlugin::eval()
{
    return lastEval;
}

/**
 * @see TriggerInterface::setConfig(const QVariantMap &)
 */
void CompassPlugin::setConfig(const QVariantMap & config) 
{
    if (   config.contains("minValue") && config.contains("maxValue")
        && config.contains("minRelation") && config.contains("maxRelation"))
    {
        minValue = config["minValue"].toInt();
        maxValue = config["maxValue"].toInt();
        minRelation = config["minRelation"].toBool();
        maxRelation = config["maxRelation"].toBool();
    }
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & CompassPlugin::config()
{
    QVariantMap * config = new QVariantMap();
    config->insert("minValue", minValue);
    config->insert("maxValue", maxValue);
    config->insert("minRelation", minRelation);
    config->insert("maxRelation", maxRelation);
    return *config;
}

/**
 * @see InputInterface::configure()
 */
bool CompassPlugin::configure()
{
    RangeInputDialog input(minValue, minRelation, maxValue, maxRelation, 0, 360);
    input.setTitle(name());
    input.setDescription("Enter the range at which this trigger should be activated");

    if (!input.exec()) {
        return false;
    }

    minValue = input.getMin();
    maxValue = input.getMax();
    minRelation = input.getMinRelation();
    maxRelation = input.getMaxRelation();
    return false;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool CompassPlugin::configure(QString)
{
    return false;
}


Q_EXPORT_PLUGIN2(compass, CompassPlugin);
