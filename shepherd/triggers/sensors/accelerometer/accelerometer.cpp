#include "accelerometer.h"
#include "../XYZInput/XYZInputDialog.h"
#include <QAccelerometer>
#include <QMessageBox>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * Initialize the sensor
 */
AccelerometerPlugin::AccelerometerPlugin(QObject* parent) : 
    QObject(parent), pix(new QPixmap), lastEval(false),
    xConfigValue(0), yConfigValue(0), zConfigValue(0),
    xGreaterThen(true), yGreaterThen(true), zGreaterThen(true)
{
    m_sensor = new QAccelerometer(this);
    m_sensor->addFilter(this);
    m_sensor->start();

}

AccelerometerPlugin::~AccelerometerPlugin() {
    delete m_sensor;
    delete pix;
}
 
/**
 * Get a reading from the accelerometer and parses it
 * @see QAccelerometerFilter::filter(QAccelerometerReading *)
 */
bool AccelerometerPlugin::filter(QAccelerometerReading* reading)
{
    if (   ((xGreaterThen && reading->x() > xConfigValue) || (!xGreaterThen && reading->x() < xConfigValue))
        && ((yGreaterThen && reading->y() > yConfigValue) || (!yGreaterThen && reading->y() < yConfigValue))
        && ((zGreaterThen && reading->z() > zConfigValue) || (!zGreaterThen && reading->z() < zConfigValue))
       ) {
        if (lastEval == false) {
            lastEval = true;
            emit stateChanged(lastEval);
            emit turnedTrue();
        }
    }
    else {
        if (lastEval == true) {
            lastEval = false;
            emit stateChanged(lastEval);
            emit turnedFalse();
        }
    }

    return false;
}

/**
 * @see TriggerInterface::eval()
 */
bool AccelerometerPlugin::eval()
{
    return lastEval;
}

/**
 * @see InfoInterface::name()
 */
QString AccelerometerPlugin::name()
{
    return QString("Accelerometer plugin");
}

/**
 * @see InfoInterface::description()
 */
QString AccelerometerPlugin::description()
{
    QString tmp;
    tmp.append("A plugin for reacting to accelerometer changes.\n");
    tmp.append("Note that this plugin probably consumes a lot of battery.");
    return tmp;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void AccelerometerPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n");
    desc.append("A accelerometer plugin by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* AccelerometerPlugin::pixmap()
{
    return pix;

}
/**
 * @see InputInterface::configure()
 */
bool AccelerometerPlugin::configure()
{
    XYZInputDialog input(xConfigValue, xGreaterThen, yConfigValue, yGreaterThen, 
            zConfigValue, zGreaterThen);
    if (!input.exec()) {
        return false;
    }

    xConfigValue = input.getX();
    yConfigValue = input.getY();
    zConfigValue = input.getZ();

    xGreaterThen = input.getXRelation();
    yGreaterThen = input.getYRelation();
    zGreaterThen = input.getZRelation();

    return true;
}

/**
 * TODO
 * @see InputInterface::configure(QString)
 */
bool AccelerometerPlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & AccelerometerPlugin::config()
{
    QVariantMap * config = new QVariantMap;
    config->insert("xConfigValue", xConfigValue);
    config->insert("yConfigValue", yConfigValue);
    config->insert("zConfigValue", zConfigValue);
    config->insert("xGreaterThen", xGreaterThen);
    config->insert("yGreaterThen", yGreaterThen);
    config->insert("zGreaterThen", zGreaterThen);
    return *config;
}

/**
 * @see TriggerInterface::setConfig()
 */
void AccelerometerPlugin::setConfig(const QVariantMap &config) 
{
    if (    config.contains("xConfigValue") && 
            config.contains("yConfigValue") && 
            config.contains("zConfigValue") && 
            config.contains("xGreaterThen") && 
            config.contains("yGreaterThen") && 
            config.contains("zGreaterThen"))
    {
        xConfigValue = config["xConfigValue"].toInt();
        yConfigValue = config["yConfigValue"].toInt();
        zConfigValue = config["zConfigValue"].toInt();

        xGreaterThen = config["xGreaterThen"].toBool();
        yGreaterThen = config["yGreaterThen"].toBool();
        zGreaterThen = config["zGreaterThen"].toBool();
    }
}
Q_EXPORT_PLUGIN2(accelerometer, AccelerometerPlugin);
