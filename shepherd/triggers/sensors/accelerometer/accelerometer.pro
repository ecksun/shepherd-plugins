#! [0]
TEMPLATE      = lib
CONFIG       += plugin
INCLUDEPATH  += ../../..
HEADERS       = accelerometer.h
SOURCES       = accelerometer.cpp
TARGET        = $$qtLibraryTarget(accelerometer)
DESTDIR       = ../../

CONFIG += console mobility
MOBILITY += sensors

#! [0]
# install
target.path = $$[QT_INSTALL_EXAMPLES]/tools/plugandpaint/plugins
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS extrafilters.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/tools/plugandpaintplugins/extrafilters
INSTALLS += target sources

symbian: include($$QT_SOURCE_TREE/examples/symbianpkgrules.pri)

symbian:TARGET.EPOCALLOWDLLDATA = 1

include(../XYZInput/XYZInput.pri)
