#ifndef SHEPHERD_ACCELEROMETER_PLUGIN_HEADER
#define SHEPHERD_ACCELEROMETER_PLUGIN_HEADER

#include "interfaces.h"

#include <QAccelerometer>
#include <QAccelerometerFilter>

QTM_USE_NAMESPACE

/**
 * \brief A accelerometer Trigger.
 *
 * A plugin trigger that utilizes the accelerometer
 */
class AccelerometerPlugin : public QObject,
                            public QAccelerometerFilter,
                            public InputInterface,
                            public InfoInterface,
                            public TriggerInterface
{
    Q_OBJECT
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(TriggerInterface)
    public:
        AccelerometerPlugin(QObject * parent = 0);
        ~AccelerometerPlugin();

        // InfoInterface
        QString name();
        QString description();
        QPixmap * pixmap();
        void aboutPlugin();

        // InputInterface
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

        // TriggerInterface
        void setConfig(const QVariantMap &);
        bool eval();

    private slots:
        bool filter(QAccelerometerReading *);

    signals:
        // TriggerInterface
        void stateChanged(bool);
        void turnedTrue();
        void turnedFalse();

    private:
        QAccelerometer * m_sensor;
        QPixmap * pix;

        bool lastEval;

        int xConfigValue;
        int yConfigValue;
        int zConfigValue;

        bool xGreaterThen;
        bool yGreaterThen;
        bool zGreaterThen;

};

#endif
