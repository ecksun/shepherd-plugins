#include "calendar.h"
#include "calendarInputDialog.h"
#include <CMulticalendar.h>
#include <CEvent.h>
#include <QDBusConnection>
#include <vector>
#include <iostream>
#include <string>
#include <QString>
#include <QDebug>
#include <QDBusConnectionInterface>
#include <QMessageBox>
#include <alarmd/libalarm.h>

using namespace Shepherd;

#define SHEPHERD_DBUS_SERVICE "org.shepherd.calendar"
#define SHEPHERD_DBUS_PATH "/shepherd/calendar"
// Make sure the _INTERFACE is in accordance with the header file
#define SHEPHERD_DBUS_INTERFACE "org.shepherd.calendar"

/**
 * Initialise the calendar
 */
CalendarPlugin::CalendarPlugin() : QObject(), pix(new QPixmap), lastState(false) 
{
    multiCalendar = CMulticalendar::MCInstance();

    // signal sender=:1.29 -> dest=(null destination) serial=40 path=/com/nokia/calendar; interface=com.nokia.calendar; member=dbChange
    //      string "CALENDAR-UI:1:EVENT:ADDED:1:6,"
    //      string "CALENDAR-UI"
    //       
    
    // Connect the signals from the calendar application to this class
    if (!QDBusConnection::sessionBus().connect("com.nokia.calendar", "/com/nokia/calendar", "com.nokia.calendar", "dbChange", this, SLOT(calendarChange(QString,QString)))) {
        qDebug() << "Could not connect to the calendar through dbus";
    }

    QDBusConnection bus = QDBusConnection::sessionBus();

    if (!bus.interface()->registerService(SHEPHERD_DBUS_SERVICE)) {
        qDebug() << "Could not register service: " << SHEPHERD_DBUS_SERVICE;
    }

    if (!bus.registerObject(SHEPHERD_DBUS_PATH, this, QDBusConnection::ExportAllSlots)) {
        qDebug() << "Could not register object: (" << SHEPHERD_DBUS_PATH << ", this, QDBusConnection::ExportAllSlots)";
    }

    setNextAlarm();
}

/**
 * Remove allocated resources
 */
CalendarPlugin::~CalendarPlugin()
{
    delete pix;
}

/**
 * @see InfoInterface::name()
 */
QString CalendarPlugin::name()
{
    return QString("Calendar plugin");
}

/**
 * @see InfoInterface::description()
 */
QString CalendarPlugin::description()
{
    return QString("A trigger plugin for calendar events");
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* CalendarPlugin::pixmap()
{
    return pix;
}

/**
 * @see TriggerInterface::setConfig()
 */
void CalendarPlugin::setConfig(const QVariantMap &config) 
{
    if (config.contains("filter") && config.contains("matchSummaryOnly")) {
        filter = QRegExp(config["filter"].toString(), Qt::CaseInsensitive);
        matchSummaryOnly  = config["matchSummaryOnly"].toBool();
    }
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & CalendarPlugin::config()
{
    QVariantMap * map = new QVariantMap();
    map->insert("filter", filter.pattern());
    map->insert("matchSummaryOnly", matchSummaryOnly);
    return *map;
}

/**
 * @see InputInterface::configure()
 */
bool CalendarPlugin::configure()
{
    CalendarInputDialog input(0, filter.pattern(), matchSummaryOnly);
    if (!input.exec()) {
        return false;
    }
    
    filter.setPattern(input.getFilter());
    matchSummaryOnly = input.getMatchSummaryOnly();
    
    return true;
}

/**
 * @see InputInterface::configure(QString)
 * TODO
 */
bool CalendarPlugin::configure(QString)
{
    return false;
}

/**
 * @see TriggerInterface::eval()
 */
bool CalendarPlugin::eval()
{
    return lastState;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void CalendarPlugin::aboutPlugin()
{
    QString desc = description();
    desc.append("\n");
    desc.append("A calendar plugin by Linus Wallgren");
    QMessageBox::about(0, name(), desc);
}


/**
 * Slot for when the calendar is changed. The arguments are ignored
 */
void CalendarPlugin::calendarChange(const QString, const QString) {
    setNextAlarm();
}

/**
 * A slot for when an event starts
 * @param id The id of the event
 */
void CalendarPlugin::eventStart(QString id)  {
    if (matches(id)) {
        emit stateChanged(true);
        emit turnedTrue();
        lastState = true;
    }
    setNextAlarm();
}

/**
 * A slot for when a event ends.
 * @param id the id of the event
 */
void CalendarPlugin::eventEnd(QString id) {
    if (matches(id)) {
        emit stateChanged(false);
        emit turnedFalse();
        lastState = false;
    }
    setNextAlarm();
}

/**
 * Clear all current alarms and set up the next one
 */
void CalendarPlugin::setNextAlarm() {
    clearAlarms();
    CEvent * nextEvent = getNextEvent();
    if (nextEvent) {
        getComponentFromId(nextEvent->getId().c_str());
        // If the event has started
        if (nextEvent->getDateStart() < time(NULL)) {
            if (matches(nextEvent->getId().c_str())) {
                emit stateChanged(true);
                emit turnedTrue();
                lastState = true;
            }
        }
        else {
            setAlarm(nextEvent->getDateStart(), "eventStart", nextEvent->getId());
        }
    }
}

/**
 * Check if the component id matches against our filter
 * This method is a wrapper around matches(CComponent *) which gets the
 * component from an id.
 * @param id The component id
 * @return true if the filter matches, false otherwise
 */
bool CalendarPlugin::matches(QString id)
{
    CComponent * component = getComponentFromId(id.toStdString());
    return matches(component);
}

/**
 * Check if the component id matches against our filter
 * @param id The component id
 * @return true if the filter matches, false otherwise
 */
bool CalendarPlugin::matches(CComponent * component)
{
    QString summary(component->getSummary().c_str());
    
    // Match title
    if (summary.contains(filter)) {
        return true;
    }
    
    // Match other parts of the component if the configurations says so
    if (!matchSummaryOnly) {
        QString description(component->getDescription().c_str());
        if (description.contains(filter)) {
            return true;
        }
    }

    return false;
}

/**
 * Get the CComponent corresponding to the provided id
 * @param id The id of the CComponent
 * @return A CComponent pointer to the component matching the id provieded,
 * NULL if nothing is found
 */
CComponent * CalendarPlugin::getComponentFromId(std::string id) {
    std::vector<string> ids;
    // I have no idea what this vector contains after the method call. On a
    // test run it contained one int, which was 1.
    std::vector<int> tmp;
    ids.push_back(id);

    int pErrorCode;
    std::vector<CComponent *> components = multiCalendar->getEventInList(ids,
            tmp, pErrorCode);

    for (std::vector<CComponent *>::iterator component = components.begin();
            component != components.end(); ++component) {
        return *component;
    }
    return NULL;
}

/**
 * Set a new alarm
 * @param alarmTime The time at which the alarm should go off
 * @param methodName The method which will be called when the alarm goes off
 * @param eventID The ID of the event which starts or stops at the alarm
 */
void CalendarPlugin::setAlarm(int alarmTime, const char * methodName, std::string eventID) {
    alarm_event_t *newEvent = 0;

    alarm_action_t *act = 0;
    
    cookie_t resultCookie;
    
    newEvent = alarm_event_create();    //Create the default alarm struct.
    
    alarm_event_set_alarm_appid(newEvent, "ShepherdCalendarPlugin");
    
    alarm_event_set_title(newEvent, "nextCalendarEvent");

    newEvent->alarm_time = (time_t) alarmTime;
        
    act = alarm_event_add_actions(newEvent, 1);
    
    act->flags = ALARM_ACTION_WHEN_TRIGGERED | ALARM_ACTION_DBUS_USE_ACTIVATION | ALARM_ACTION_TYPE_DBUS;
    
    alarm_action_set_dbus_interface(act, SHEPHERD_DBUS_INTERFACE);
    alarm_action_set_dbus_service(act, SHEPHERD_DBUS_SERVICE);
    alarm_action_set_dbus_path(act, SHEPHERD_DBUS_PATH);
    alarm_action_set_dbus_name(act, methodName);

    std::string arr[] = {eventID};
    // alarm_action_set_dbus_args wants an array..
    alarm_action_set_dbus_args(act, 's', arr, NULL);
        
    resultCookie = alarmd_event_add(newEvent);
    alarm_event_delete(newEvent);
    
    alarms.push_back(resultCookie);
}

/**
 * Clear all previous alarms
 */
void CalendarPlugin::clearAlarms() {
    for (std::vector<cookie_t>::iterator alarm = alarms.begin(); alarm !=
            alarms.end(); ++alarm) {
        if (alarmd_event_del(*alarm) != 0) {
            qDebug() << "Error while trying to delete an event";
        }
    }
    alarms.clear();
}


/**
 * Returns the events currently happening that matches the specified arguments.
 *
 * @param summary The summary/name of the event to search for, if the string is
 * empty everything is matched
 * @param description The description of the event to find, if the parameter is
 * empty everything is matched 
 * @param location The position of the event, if it
 * is empty everything is matched
 *
 * @return A list of currently ongoing matching events
 */
std::vector<CEvent *> CalendarPlugin::getEvents(QString summary, QString description,
        QString location) {
    std::vector<CCalendar *> calendars = multiCalendar->getListCalFromMc();

    std::vector<CEvent *> returnList;

    for (std::vector<CCalendar *>::iterator it = calendars.begin(); it !=
            calendars.end(); ++it) {

        int err = 4711;
        std::vector< CEvent * > events = (*it)->getEvents(err);

        for (std::vector<CEvent *>::iterator event = events.begin();
                event != events.end(); ++event) {
            // Is the event currently happening?
            qDebug() << (*event)->getSummary().c_str() << ": " << (*event)->getDateStart() << " < " << time(NULL) << " && " << (*event)->getDateEnd() << " > " << time(NULL);
            if ((*event)->getDateStart() < time(NULL) && (*event)->getDateEnd() > time(NULL)) {
                QString sum((*event)->getSummary().c_str());
                QString desc((*event)->getDescription().c_str());
                QString loc((*event)->getLocation().c_str());

                if ( (summary.isEmpty()       || sum.toLower().contains(summary.toLower()) ) &&
                     (description.isEmpty()   || desc.toLower().contains(description.toLower())) &&
                     (location.isEmpty()      || loc.toLower().contains(location.toLower()) ) ) {

                    returnList.push_back(*event);
                }
            }
        }

        // Couldnt find any reference defining what the error codes actually
        // mean, but 500 seems to be good :)
        if (err != 500) {
            qDebug() << "pErrorCode: " << err;
        }
    }
    multiCalendar->releaseListCalendars(calendars);
    return returnList;
}

/**
 * Return the next upcoming event.
 *
 * @return The next event, NULL if there is no upcoming events
 */
CEvent * CalendarPlugin::getNextEvent() {
    std::vector<CCalendar *> calendars = multiCalendar->getListCalFromMc();

    // std::vector<CEvent *> returnList;
    int nextTime = 0;
    int anyEventChecked = false;
    CEvent * nextEvent = NULL;

    for (std::vector<CCalendar *>::iterator it = calendars.begin(); it !=
            calendars.end(); ++it) {
        
        int err = 4711;
        std::vector< CEvent * > events = (*it)->getEvents(err);

        for (std::vector<CEvent *>::iterator event = events.begin();
                event != events.end(); ++event) {
            // Has the event happened yet?
            if ((*event)->getDateEnd() > time(NULL)) {
                if (anyEventChecked == false || (*event)->getDateStart() < nextTime) {
                    anyEventChecked = true;
                    nextTime = (*event)->getDateStart();
                    nextEvent = *event;
                }
            }
        }

        // Couldnt find any reference defining what the error codes actually
        // mean, but 500 seems to be good :)
        if (err != 500) {
            qDebug() << "pErrorCode: " << err;
        }
    }
    multiCalendar->releaseListCalendars(calendars);
    return nextEvent;
}

Q_EXPORT_PLUGIN2(calendar, CalendarPlugin);
