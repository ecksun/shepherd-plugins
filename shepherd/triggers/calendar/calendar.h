#ifndef SHEPHERD_CALENDAR_H
#define SHEPHERD_CALENDAR_H

#include <QObject>
#include <QDBusMessage>

#include <CMulticalendar.h>
#include <QString>
#include <CEvent.h>

#include <alarmd/libalarm.h>

#include "interfaces.h"

#include <vector>

namespace Shepherd {
    /**
     * \brief A calendar Trigger
     *
     * A plugin that looks at the calendar events and matches them against a user
     * defined filter 
     */
    class CalendarPlugin : public QObject,
    public InfoInterface,
                     public InputInterface,
                     public TriggerInterface
    {
        Q_OBJECT
        Q_INTERFACES(InfoInterface)
        Q_INTERFACES(InputInterface)
        Q_INTERFACES(TriggerInterface)
        
        // Perhaps this should be replaced by a XML file
        Q_CLASSINFO("D-Bus Interface", "org.shepherd.calendar")

        public:
            CalendarPlugin();
            ~CalendarPlugin();
            
            QString name();
            QString description();
            QPixmap * pixmap();
            void aboutPlugin();

            void setConfig(const QVariantMap &);

            bool configure();
            bool configure(QString);
            const QVariantMap & config();

            bool eval();

            std::vector<CEvent *> getEvents(QString, QString, QString);
            CEvent * getNextEvent();
            CComponent * getComponentFromId(std::string);
            
        private:
            CMulticalendar * multiCalendar;
            QRegExp filter;
            QPixmap * pix;
            bool lastState;
            bool matchSummaryOnly;
            void setNextAlarm();
            void setAlarm(int, const char *, std::string);
            std::vector<cookie_t> alarms;
            void clearAlarms();
            bool matches(QString);
            bool matches(CComponent *);

        signals:
            void stateChanged(bool);
            void turnedTrue();
            void turnedFalse();
            
        public slots:
            void calendarChange(const QString arg1, const QString arg2);
            void eventStart(QString);
            void eventEnd(QString);
    };
}

#endif
