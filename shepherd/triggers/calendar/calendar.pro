#! [0]
TEMPLATE      = lib
CONFIG       += plugin
INCLUDEPATH  += ../..
HEADERS       = calendar.h calendarInputDialog.h
SOURCES       = calendar.cpp calendarInputDialog.cpp
TARGET        = $$qtLibraryTarget(calendar)
DESTDIR       = ../

CONFIG += dbus

CONFIG += link_pkgconfig
PKGCONFIG += calendar-backend

QT += dbus

#! [0]
# install
target.path = $$[QT_INSTALL_EXAMPLES]/tools/plugandpaint/plugins
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS extrafilters.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/tools/plugandpaintplugins/extrafilters
INSTALLS += target sources

symbian: include($$QT_SOURCE_TREE/examples/symbianpkgrules.pri)

symbian:TARGET.EPOCALLOWDLLDATA = 1

FORMS += calendar.ui
