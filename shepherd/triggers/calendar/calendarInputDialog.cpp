#include "ui_calendar.h"
#include "calendarInputDialog.h"

/**
 * Setup the UI, set the default checkboxes/texts and connect the slots with
 * the appropriate signals
 */
CalendarInputDialog::CalendarInputDialog(QWidget *parent, QString prevFilter, 
        bool prevMatchSummaryOnly) :
    QDialog(parent), filter(prevFilter), matchSummaryOnly(prevMatchSummaryOnly)
{

    setupUi(this);

    matchSummaryBox->setChecked(matchSummaryOnly);
    filterLine->setText(filter);

    connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
}

/**
 * Return what box was checked in the dialog
 * @return True if the user wants to match the summary only
 */
bool CalendarInputDialog::getMatchSummaryOnly()
{
    return matchSummaryOnly;
}

/**
 * Get the filter associated the user has inputed into the dialog
 */
QString CalendarInputDialog::getFilter()
{
    return filter;
}

/**
 * The slot that is called when the save button is pushed
 * This method saves the current state and closes the dialog
 */
void CalendarInputDialog::save()
{
    matchSummaryOnly = matchSummaryBox->isChecked();
    filter = filterLine->text();
    this->accept();
}
