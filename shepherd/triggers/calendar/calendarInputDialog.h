#ifndef SHEPHERD_CALENDAR_INPUT_H
#define SHEPHERD_CALENDAR_INPUT_H

#include <QtGui/QDialog>
#include "ui_calendar.h"

/**
 * \brief Calendar filter input dialog
 *
 * A input dialog for asking the user about what calendar filters to use
 */
class CalendarInputDialog : public QDialog, public Ui::calendarDialog
{
    Q_OBJECT
    public:
        explicit CalendarInputDialog(QWidget *, QString, bool);
        bool getMatchSummaryOnly();
        QString getFilter();
        
    public slots:
        void save();

    private:
        QString filter;
        bool matchSummaryOnly;

};

#endif

