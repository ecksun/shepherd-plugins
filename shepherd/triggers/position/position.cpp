#include "position.h"
#include <QGeoPositionInfoSource>
#include <QGeoAreaMonitor>
#include <QMessageBox>
#include <QDebug>

/**
 * Initiate the positioning service
 */
Position::Position() : QObject(), lastState(false), pix(new QPixmap()),
                       monitor(0), source(0) {
}


/**
 * Remove allocated resources
 */
Position::~Position()
{
    delete pix;
}

/**
 * @see InfoInterface::name()
 */
QString Position::name()
{
    return QString("Position trigger");
}

/**
 * @see InfoInterface::description()
 */
QString Position::description()
{
    return QString("A position dependant trigger.");
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* Position::pixmap()
{
    return pix;
}

/**
 * @see TriggerInterface::setConfig()
 */
void Position::setConfig(const QVariantMap &config) 
{
    if (config.contains("method") && config["method"].type() == QVariant::String &&
            config.contains("distance") && config["distance"].type() == QVariant::UInt) {
        if (config.contains("latitude") && config.contains("longitude") && config.contains("altitude")) {
            configPosition = QGeoCoordinate(config["latitude"].toDouble(), config["longitude"].toDouble(), config["altitude"].toDouble());

            if (config["method"].toString().compare("monitor")) {
                useSelfMonitor();
            }
            else {
                useAreaMonitor();
            }
        }
    }
    // Lets make us a copy
    configMap = new QVariantMap(config);
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & Position::config()
{
    return *configMap;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void Position::aboutPlugin()
{
    QMessageBox::about(0, "Position trigger", 
            "A geographical trigger both with and without the GPS, by Linus Wallgren");
}

/**
 * Evaluate the current conditions.
 */
bool Position::eval()
{
    // We need a proper configuration
    if (!configPosition.isValid()) {
        return false;
    }
    QGeoCoordinate position;
    if (configMap->contains("method") &&
            (*configMap)["method"].toString().compare("monitor")) {
        position = source->lastKnownPosition().coordinate();
    }
    else {
        position = monitor->center();
    }
    if (position.distanceTo(configPosition) <= (*configMap)["distance"].toInt()) {
        lastState = true;
        return true;
    }
    return false;
}

/**
 * Use changes in the position to find out where the position is
 */
void Position::useSelfMonitor() {
    source = QGeoPositionInfoSource::createDefaultSource(0);

    if (source) {
        // Dont use GPS
        source->setPreferredPositioningMethods(QGeoPositionInfoSource::NonSatellitePositioningMethods);

        // Connect the signal to our slot
        connect(source, SIGNAL(positionUpdated(QGeoPositionInfo)),
                this, SLOT(positionUpdated(QGeoPositionInfo)));

        // Start the updates
        source->startUpdates();
    }
    else {
        qDebug() << "error! sadface";
    }

}

/*
 * Use an areaMonitor to look for changes in position
 *
 * NOTE:
 * I can't find a way to specify the method used when QGeoAreaMonitor gets the
 * position, I would like it to NOT use GPS to save power.
 * It might be so that QGeoAreaMonitor is smart and only uses the GPS when its
 * nescesary (the current position is near the border of the area). 
 *
 * TODO: find out how QGeoAreaMonitor works
 */
void Position::useAreaMonitor() {
    monitor = QGeoAreaMonitor::createDefaultMonitor(0);
    connect(monitor, SIGNAL(areaEntered(QGeoPositionInfo)),
            this, SLOT(areaEntered(QGeoPositionInfo)));
    connect(monitor, SIGNAL(areaExited(QGeoPositionInfo)),
            this, SLOT(areaExited(QGeoPositionInfo)));

    monitor->setCenter(configPosition);
    monitor->setRadius((*configMap)["distance"].toInt());
}

/**
 * Slot for when the position is updated
 *
 * @param info The QGeoPositionInfo object associated with the event
 */
void Position::positionUpdated(const QGeoPositionInfo &position) {
    if (position.coordinate().distanceTo(configPosition) <=
            (*configMap)["distance"].toInt()) {
        if (lastState == false) {
            emit stateChanged(true);
            emit turnedTrue();
            lastState = true;
        }
    }
    else if (lastState == true) {
        emit stateChanged(false);
        emit turnedFalse();
        lastState = false;
    }
}

/**
 * Slot for when an area is entered
 *
 * @param info The QGeoPositionInfo object associated with the event
 */
void Position::areaEntered(const QGeoPositionInfo &) {
    if (lastState == false) {
        emit stateChanged(true);
        emit turnedTrue();
        lastState = true;
    }
}

/**
 * Slot for when an area is exited
 *
 * @param info The QGeoPositionInfo object associated with the event
 */
void Position::areaExited(const QGeoPositionInfo &) {
    if (lastState == true) {
        emit stateChanged(false);
        emit turnedFalse();
        lastState = false;
    }
}
