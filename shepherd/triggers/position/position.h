#ifndef SHEPHERD_POSITION_H
#define SHEPHERD_POSITION_H

#include <QObject>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QGeoAreaMonitor>
#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief A geographical position Trigger.
 *
 * A plugin for triggering on the position of the device
 * There is two ways to find out the current position, either via forcing the
 * device to only use the cellular towers for location or to use a
 * QGeoAreaMonitor
 */
class Position : public QObject, public InfoInterface, 
                 public TriggerInterface 
{
    Q_OBJECT
    Q_INTERFACES(InfoInterface)
    // Q_INTERFACES(InputInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        Position();
        ~Position();
        QString name();
        QString description();
        QPixmap* pixmap();
        bool configure();
        bool configure(QString cfg);
        const QVariantMap& config();
        void setConfig(const QVariantMap&);
        bool eval();
        
    signals:
        void stateChanged(bool b);
        void turnedTrue();
        void turnedFalse();
    public slots:
        void aboutPlugin();

    private:
        void useSelfMonitor();
        void useAreaMonitor();
        bool lastState;
        QPixmap * pix;
        QVariantMap * configMap;
        QGeoAreaMonitor * monitor;
        QGeoPositionInfoSource * source;
        QGeoCoordinate configPosition;

    private slots:
        void positionUpdated(const QGeoPositionInfo &);
        void areaEntered(const QGeoPositionInfo &);
        void areaExited(const QGeoPositionInfo &);
        
};

#endif
