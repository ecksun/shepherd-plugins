#ifndef SHEPHERD_PLUGIN_WLAN_LISTNER_H
#define SHEPHERD_PLUGIN_WLAN_LISTNER_H

#include <QObject>
#include <QNetworkConfigurationManager>
#include "interfaces.h"

QTM_USE_NAMESPACE

/**
 * \brief
 * Action plugin that looks for networks
 *
 * This plugin watches for any available networks.
 */
class WlanListner : public QObject, public InfoInterface, 
                    public InputInterface, 
                    public TriggerInterface 
{

    Q_OBJECT
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(TriggerInterface)

    public:
        WlanListner();
        ~WlanListner();
        QString name();
        QString description();
        QPixmap* pixmap();
        bool configure();
        bool configure(QString cfg);
        const QVariantMap& config();
        void setConfig(const QVariantMap&);
        bool eval();

    signals:
        void stateChanged(bool b);
        void turnedTrue();
        void turnedFalse();
    public slots:
        void aboutPlugin();

    private slots:
        void    configurationAdded ( const QNetworkConfiguration & config );
        void    configurationChanged ( const QNetworkConfiguration & config );
        void    configurationRemoved ( const QNetworkConfiguration & configuration );
        void    onlineStateChanged ( bool isOnline );
        void    updateCompleted ();
    private:
        QNetworkConfigurationManager manager;
        bool lastState;
        void print(const QNetworkConfiguration &);
        bool isConfigAvailable();
        bool matches(const QNetworkConfiguration &);
        QPixmap * pix;
        QString ssid;
};


#endif
