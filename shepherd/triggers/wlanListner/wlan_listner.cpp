#include <QNetworkConfigurationManager>
#include <QDebug>
#include <iostream>
#include "wlan_listner.h"
#include <QMessageBox>
#include <QInputDialog>

/**
 * Connects the interesting signals to their corresponding slots
 */
WlanListner::WlanListner() : QObject(), pix(new QPixmap()) {
    /*
     * Connect the signals from QNetworkConfigurationManager to the slots of
     * this class
     */
    connect(&manager, SIGNAL(configurationAdded(const QNetworkConfiguration&)),
            this, SLOT(configurationAdded(const QNetworkConfiguration&)));

    connect(&manager, SIGNAL(configurationRemoved(const QNetworkConfiguration&)),
            this, SLOT(configurationRemoved(const QNetworkConfiguration&)));

    connect(&manager, SIGNAL(configurationChanged(const QNetworkConfiguration&)),
            this, SLOT(configurationChanged(const QNetworkConfiguration)));

    connect(&manager, SIGNAL(updateCompleted()), this, SLOT(updateCompleted()));

    connect(&manager, SIGNAL(onlineStateChanged(bool)), this ,SLOT(onlineStateChanged(bool)));

}

/**
 * Remove allocated resources
 */
WlanListner::~WlanListner()
{
    delete pix;
}

/**
 * @see InfoInterface::name()
 */
QString WlanListner::name()
{
    return QString("WLAN listner");
}

/**
 * @see InfoInterface::description()
 */
QString WlanListner::description()
{
    return QString("A plugin for triggers on wlan ssid");
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap* WlanListner::pixmap()
{
    return pix;
}

/**
 * @see TriggerInterface::setConfig()
 */
void WlanListner::setConfig(const QVariantMap &config) 
{
    if (config.contains("ssid")) {
        ssid = config["ssid"].toString();
    }
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & WlanListner::config()
{
    QVariantMap * map = new QVariantMap();
    map->insert("ssid", ssid);
    return *map;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void WlanListner::aboutPlugin()
{
    QMessageBox::about(0, "WLAN trigger", "A trigger on network configuration changes by Linus Wallgren");
}

/**
 * Evaluate the current conditions.
 *
 * Note that this method updates the current configurations, however it does
 * not wait for it to finish but goes through the configurations anyway. This
 * might give false results.
 *
 * @return True if a network matching the config is found
 */
bool WlanListner::eval()
{
    manager.updateConfigurations();
    return isConfigAvailable();
}


/**
 * Check if a network matching the config is found
 *
 * @return True if a network matching the config is found.
 */
bool WlanListner::isConfigAvailable()
{
    QList<QtMobility::QNetworkConfiguration> configs = manager.allConfigurations();
    while (!configs.isEmpty()) {
        QtMobility::QNetworkConfiguration config = configs.takeFirst();
        // TODO find out where the ssid is saved
        if (matches(config)) {
            lastState = true;
            return true;
        }
    }

    lastState = false;
    return false;
}

/**
 * Helper method to compare the network to the config. Written if it is
 * nescsesary to change the way the comparison is done in order to get more
 * accuracy.
 *
 * @param config The QNetworkConfiguration we are going to compare to the
 * config
 * @return true if the argument matches the config
 */
bool WlanListner::matches(const QNetworkConfiguration &config) 
{
    return (config.isValid() && (config.name().compare(ssid) == 0));
}

/**
 * Method called when a configuration is added
 */
void WlanListner::configurationAdded(const QNetworkConfiguration &config) 
{
    if (matches(config)) {
        emit stateChanged(false);
        emit turnedFalse();
        lastState = false;
    }
}

/**
 * The method called when a configuration is changed
 */
void WlanListner::configurationChanged(const QNetworkConfiguration &config) 
{
    if (matches(config)) {
        if (lastState == false) {
            emit stateChanged(true);
            emit turnedTrue();
        }
    }
}

/**
 * Method called when a configuration is removed
 */
void WlanListner::configurationRemoved(const QNetworkConfiguration &config) 
{
    if (matches(config)) {
        emit stateChanged(false);
        emit turnedFalse();
    }
}

/**
 * Method called when the online state changes. Meaning the system either goes
 * online or offline
 */
void WlanListner::onlineStateChanged(bool) 
{
    if (isConfigAvailable()) {
        if (lastState == false) {
            emit stateChanged(true);
            emit turnedTrue();
        }
    }
    else if (lastState == true) {
        emit stateChanged(false);
        emit turnedFalse();
    }
}

/**
 * Method called when the update is completed, for example after the
 * updateConfigurations() is called
 */
void WlanListner::updateCompleted() 
{
    if (isConfigAvailable()) {
        if (lastState == false) {
            emit stateChanged(true);
            emit turnedTrue();
        }
    }
    else if (lastState == true) {
        emit stateChanged(false);
        emit turnedFalse();
    }
}

/**
 * @see InputInterface::configure
 */
bool WlanListner::configure()
{
    QStringList networks;
    int currentNetwork = 0;
    QList<QtMobility::QNetworkConfiguration> configs = manager.allConfigurations();
    int i = 0;
    while (!configs.isEmpty()) {
        QtMobility::QNetworkConfiguration config = configs.takeFirst();
        if (!config.isValid()) {
            continue;
        }
        networks.append(config.name());
        if (config.name().compare(ssid) == 0) {
            currentNetwork = i;
        }
        ++i;
    }
    
    bool ok;
    QString network = QInputDialog::getItem(0, "Network configuration", 
            "Choose network configuration", networks, currentNetwork, false, &ok);

    // In case the user actually accepted the dialog and not canceled it
    if (ok) {
        ssid = network;
        return true;
    }
    return false;
}

/**
 * @see InputInterface::configure(QString)
 * TODO
 */
bool WlanListner::configure(QString)
{
    return false;
}

Q_EXPORT_PLUGIN2(wlanListner, WlanListner)
