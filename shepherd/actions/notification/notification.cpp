/*
 * Apperently #include<libnotify/notify.h> needs to be defined first because of
 * some #includes conflicts between Qt and libnotify
 *
 * See
 * http://discussion.forum.nokia.com/forum/showthread.php?190686-how-can-i-display-notification-on-N900-screen
 * for more information
 */
#include <libnotify/notify.h>

#include <notification.h>
#include <QDebug>
#include <QMessageBox>
#include <QInputDialog>

/**
 * Initialize a QPixmap, message an libnotify
 */
NotificationPlugin::NotificationPlugin() 
{
    pix = new QPixmap();

    message = "Enter the notification you would like to see.";

    // Init libnotify library
    if (!notify_init("shepherd")) {
        qDebug() << "Could not initialize";
    }
}

/**
 * Uninitialize libnotify.
 * According to the libnotify documentation this should only be run when
 * libnotify is not going to be used in the future:
 * "This should be called when the program no longer needs libnotify for the
 * rest of its lifecycle, typically just before exitting."
 *
 * @see http://maemo.org/api_refs/5.0/5.0-final/libnotify/libnotify-notify.html#notify-uninit
 *
 */
NotificationPlugin::~NotificationPlugin()
{
    delete pix;
    notify_uninit();
}

/**
 * @see InfoInterface::name()
 */
QString NotificationPlugin::name() 
{
    return QString("Notification");
}

/**
 * @see InfoInterface::description
 */
QString NotificationPlugin::description() 
{
    return QString("Display a notification");    
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap * NotificationPlugin::pixmap()
{
    return pix;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void NotificationPlugin::aboutPlugin()
{
    QMessageBox::about(0, "Notification", "Display a notification. By ecksun (Linus Wallgren).");
}

/**
 * Display the notification
 */
bool NotificationPlugin::execute()
{
    if (!notify_is_initted()) {
        qDebug() << "libnotify is not initiated";
        return false;
    }
    NotifyNotification *notification;

    // Create notification
    notification = notify_notification_new("shepherd",
            message.toString().toStdString().c_str() , NULL, NULL);

    if (notification) {
        // Set timeout
        notify_notification_set_timeout(notification, 3000);

        // Schedule notification for showing
        if (!notify_notification_show(notification, NULL)) {
            qDebug("Failed to send notification");
            return false;
        }

        // Clean up the memory
        g_object_unref(notification);
        return true;
    } 
    else {
        qDebug("Failed to create notification");
        return false;
    }
    return false;
}

/**
 * @see ActionInterface::setConfig()
 */
void NotificationPlugin::setConfig(const QVariantMap & config) {
    if (config.contains("message")) {
        this->message = config["message"];

        if (this->message.toString().isEmpty()) {
            qDebug() << "Message is empty";
        }
    }
}

/**
 * @see InputInterface::configure()
 */
bool NotificationPlugin::configure()
{
    bool ok;
    QString text = QInputDialog::getText(0, "Notification",
            "Text to display", QLineEdit::Normal, message.toString(), &ok);

    message = text;

    if (ok) {
        return true;
    }
    return false;
}

/**
 * TODO Will need the format of the argument in order to implement this
 * @see InputInterface::configure(QString)
 */
bool NotificationPlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & NotificationPlugin::config()
{
    QVariantMap * map = new QVariantMap();
    map->insert("message", message);

    return *map;
}

Q_EXPORT_PLUGIN2(notification, NotificationPlugin)
