#ifndef SHEPHERD_NOTIFICATION_H
#define SHEPHERD_NOTIFICATION_H

#include "interfaces.h"
#include <QObject>
#include <QVariant>
#include <string>

/**
 * \brief
 * Action plugin that displays a notification
 */
class NotificationPlugin :  public QObject, 
                            public InfoInterface, 
                            public InputInterface,
                            public ActionInterface
{
    Q_OBJECT
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(ActionInterface)

    public:
        NotificationPlugin();
        ~NotificationPlugin();
        QString name();
        QString description();
        QPixmap * pixmap();
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

    public slots:
        bool execute();
        void setConfig(const QVariantMap &);
        void aboutPlugin();

    private:
        QVariant message;
        QPixmap * pix;

};

#endif
