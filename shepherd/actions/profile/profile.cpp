#include <libprofile.h>
#include "profile.h"
#include <QObject>
#include <QMessageBox>
#include <QDebug>
#include <QInputDialog>

/**
 * Initialize a pixmap
 */
ProfilePlugin::ProfilePlugin() : QObject() 
{
    pix = new QPixmap();
}

/**
 * Remove the pixmap
 */
ProfilePlugin::~ProfilePlugin()
{
    delete pix;
}

/**
 * @see InfoInterface::name()
 */
QString ProfilePlugin::name() 
{
    return QString("Profile");
}

/**
 * @see InfoInterface::description()
 */
QString ProfilePlugin::description() 
{
    return QString("Change the profile of the device");
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap * ProfilePlugin::pixmap()
{
    return pix;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void ProfilePlugin::aboutPlugin()
{
    QMessageBox::about(0, "Profile", "Shepherd plugin to change the profile, by ecksun (Linus Wallgren).");
}

/**
 * @see ActionInterface::execute()
 */
bool ProfilePlugin::execute() {
    if (!profile_set_profile(profile.toString().toStdString().c_str())) {
        qDebug() << "profile_set_profile() gave an error.";
        return false;
    }
    return true;
}

/**
 * @see ActionInterface::setConfig()
 */
void ProfilePlugin::setConfig(const QVariantMap & config) {
    if (config.contains("profile")) {
        this->profile = config["profile"];
    }
    else {
        // fallback incase of something goes wrong
        this->profile = "general";
    }
}

/**
 * @see InputInterface::configure()
 */
bool ProfilePlugin::configure()
{
    QStringList profileList;
    // Get all available profiles
    char ** profiles = profile_get_profiles();

    for (int i = 0; profiles[i] != 0; ++i) {
        profileList.append(profiles[i]);    
    }

    profile_free_profiles(profiles);

    bool ok = false;
    QString profile = QInputDialog::getItem(0, "Profile", "Choose profile", profileList, 0, false, &ok);
    
    if (ok) {
        this->profile = profile;
        return true;
    }
    return false;
}

/**
 * @see InputInterface::configure(QString)
 */
bool ProfilePlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config()
 */
const QVariantMap & ProfilePlugin::config()
{
    QVariantMap * map = new QVariantMap();
    map->insert("profile", profile);
    return *map;
}

Q_EXPORT_PLUGIN2(profile, ProfilePlugin);
