#ifndef SHEPHERD_PROFILE_H
#define SHEPHERD_PROFILE_H

#include "interfaces.h"
#include <QVariant>


/**
 * \brief
 * Action plugin for changing the profile
 */
class ProfilePlugin :  public QObject,
                    public InfoInterface,
                    public InputInterface,
                    public ActionInterface
{
    Q_OBJECT
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(ActionInterface)

    public:
        ProfilePlugin();
        ~ProfilePlugin();
        QString name();
        QString description();
        QPixmap * pixmap();
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

    private:
        QVariant profile;
        QPixmap * pix;


    public slots:
        bool execute();
        void setConfig(const QVariantMap &);
        void aboutPlugin();
};

#endif
