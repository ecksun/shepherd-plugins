#include "ui_cellular.h"
#include "cellularPluginInputDialog.h"

/**
 * Setup the UI and connect the appropriate signals to their corresponding slots
 */
CellularPluginInputDialog::CellularPluginInputDialog(QWidget * parent, bool network) : 
    QDialog(parent), networkStatus(network)
{
    setupUi(this);
    connect(radioOffButton, SIGNAL(clicked()), this, SLOT(radioOff()));
    connect(radioOnButton, SIGNAL(clicked()), this, SLOT(radioOn()));
}

/**
 * Tells us if the network should be turned on or off
 * @return True if the radio should be turned on
 */
bool CellularPluginInputDialog::networkOn()
{
    return networkStatus;
}

/**
 * The slot that is called when the user presses the button that tells us that
 * we should turn the radio off
 */
void CellularPluginInputDialog::radioOff()
{
    networkStatus = false;
    this->done(QDialog::Accepted);
}

/**
 * The slot that is called when the user presses the button that tells us that
 * we should turn the radio on
 */
void CellularPluginInputDialog::radioOn()
{
    networkStatus = true;
    this->done(QDialog::Accepted);
}

