#ifndef SHEPHERD_CELLULAR_INPUT_DIALOG_H
#define SHEPHERD_CELLULAR_INPUT_DIALOG_H

#include <QtGui/QDialog>
#include "ui_cellular.h"

/**
 * \brief input dialog asking weather to turn the cellular network on or off
 *
 * The dialog for askng the user to either turn the cellular network on or off
 */
class CellularPluginInputDialog : public QDialog, 
                                  public Ui::cellularDialog
{
    Q_OBJECT
    public:
        explicit CellularPluginInputDialog(QWidget * parent = 0, bool networkOn = true);
        bool networkOn();

    public slots:
        void radioOff();
        void radioOn();

    private:
        bool networkStatus;

};

#endif
