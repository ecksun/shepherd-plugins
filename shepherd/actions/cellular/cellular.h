#ifndef SHEPHERD_CELLULAR_H
#define SHEPHERD_CELLULAR_H

#include <QObject>
#include "interfaces.h"

/**
 * \brief Action for offline/tablet mode
 *
 * A plugin for going in and out of offline/tablet mode
 */
class CellularPlugin :    public QObject,
                          public InfoInterface,
                          public InputInterface,
                          public ActionInterface
{
    Q_OBJECT
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(ActionInterface)

    public:
        CellularPlugin();
        ~CellularPlugin();
        QString name();
        QString description();
        QPixmap * pixmap();
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

    private:
        QPixmap * pix;
        bool cellularNetworkOn;


    public slots:
        bool execute();
        void setConfig(const QVariantMap &);
        void aboutPlugin();
};

#endif
