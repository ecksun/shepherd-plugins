#include "cellular.h"
#include "cellularPluginInputDialog.h"
#include <QDBusConnection>
#include <QDBusMessage>
#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include <QDBusArgument>
#include <QMessageBox>

CellularPlugin::CellularPlugin() : QObject(), cellularNetworkOn(true) {
    pix = new QPixmap();
}

CellularPlugin::~CellularPlugin()
{
    delete pix;
}


/**
 * @see InfoInterface::name()
 */
QString CellularPlugin::name() 
{
    return QString("Cellular switcher");
}

/**
 * @see InfoInterface::description()
 */
QString CellularPlugin::description() 
{
    return QString("Turn the cellular network on or off");
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap * CellularPlugin::pixmap()
{
    return pix;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void CellularPlugin::aboutPlugin()
{
    QMessageBox::about(0, name(), "Plugin to turn the cellular network on or off, by ecksun (Linus Wallgren).");
}

/**
 * @see ActionInterface::execute()
 */
bool CellularPlugin::execute() 
{
    QList<QVariant> radioArguments;
    radioArguments.append(cellularNetworkOn);

    QDBusMessage message =
        QDBusMessage::createMethodCall("com.nokia.phone.SSC",
                "/com/nokia/phone/SSC", "com.nokia.phone.SSC", "set_radio");
    message.setArguments(radioArguments);

    QDBusMessage result = QDBusConnection::systemBus().call(message, QDBus::NoBlock);

    if (result.errorName().isEmpty()) {
        return true;
    }
    else {
        return false;
    }
}

/**
 * @see ActionInterface::setConfig()
 */
void CellularPlugin::setConfig(const QVariantMap & config) 
{
    if (config.contains("cellularNetworkOn")) {
        cellularNetworkOn = config["cellularNetworkOn"].toBool();
    }
    else {
        cellularNetworkOn = true;
    }
}

/**
 * @see InputInterface::configure()
 */
bool CellularPlugin::configure()
{
    CellularPluginInputDialog dialog;
    if (!dialog.exec()) {
        return false;
    }
    cellularNetworkOn = dialog.networkOn();
    return true;
}

/**
 * @see InputInterface::configure(QString)
 * TODO
 */
bool CellularPlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config
 */
const QVariantMap & CellularPlugin::config() 
{
    QVariantMap * map = new QVariantMap();
    map->insert("cellularNetworkOn", cellularNetworkOn);
    return *map;
}

Q_EXPORT_PLUGIN2(cellular, CellularPlugin);
