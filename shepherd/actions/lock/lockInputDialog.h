#ifndef SHEPHERD_LOCK_UI_H
#define SHEPHERD_LOCK_UI_H

#include <QtGui/QDialog>
#include "ui_lock.h"

namespace Ui 
{
    class LockInputDialog;
}   

/**
 * \brief
 * A dialog for the user to enter wether to lock or unlock the device
 */
class LockInputDialog : public QDialog, public Ui::lockDialog
{
    Q_OBJECT
    public:
        explicit LockInputDialog(QWidget *parent = 0);
        bool isLocking();

    public slots:
        void save();

    private:
        bool locking;
};

#endif


