#ifndef SHEPHERD_LOCK_H
#define SHEPHERD_LOCk_H

#include "interfaces.h"
#include <QObject>
#include <QVariant>
#include <QDBusMessage>


/**
 * \brief 
 * Action plugin that locks and unlocks the device
 */
class LockPlugin :  public QObject,
                    public InfoInterface,
                    public InputInterface,
                    public ActionInterface
{
    Q_OBJECT
    Q_INTERFACES(InfoInterface)
    Q_INTERFACES(InputInterface)
    Q_INTERFACES(ActionInterface)

    public:
        LockPlugin();
        ~LockPlugin();
        void lock();
        void unlock();
        QString name();
        QString description();
        QPixmap * pixmap();
        bool configure();
        bool configure(QString);
        const QVariantMap & config();

    private:
        bool locking;
        QList<QVariant> arguments;
        QDBusMessage message;
        QPixmap * pix;


    public slots:
        bool execute();
        void setConfig(const QVariantMap &);
        void aboutPlugin();
};

#endif
