#include "lock.h"
#include "lockInputDialog.h"
#include <QDBusConnection>
#include <QDBusMessage>
#include <QCoreApplication>
#include <QStringList>
#include <QMessageBox>
#include <QDebug>

/**
 * Handle and initiate the right arguments for the DBus message
 * 
 * Initiate the arguments needed for the DBus message that are going to be used
 * to lock and unlock the device The constructor does also take care of the
 * argument handling.
 */
LockPlugin::LockPlugin() : QObject() 
{
    locking = false;
    pix = new QPixmap();

    // the following arguments are the same independent of locking or unlocking
    // the device
    arguments.append("com.nokia.mce");
    arguments.append("/com/nokia/mce/request");
    arguments.append("com.nokia.mce.request");
    arguments.append("devlock_callback");
}

LockPlugin::~LockPlugin() 
{
    delete pix;
}

/**
 * Prepare the QDBusMessage for locking the device
 */
void LockPlugin::lock() 
{
    message = QDBusMessage::createMethodCall("com.nokia.system_ui",
            "/com/nokia/system_ui/request", "com.nokia.system_ui.request",
            "devlock_open");
    
    // The 5th argument
    arguments.insert(4, (unsigned int)3);
    execute();
}

/**
 * Prepare the QDBusMessage for unlocking the deivce
 */
void LockPlugin::unlock() 
{
    message = QDBusMessage::createMethodCall("com.nokia.system_ui",
            "/com/nokia/system_ui/request", "com.nokia.system_ui.request",
            "devlock_close");
    // The 5th argument
    arguments.insert(4, (unsigned int)0);
    execute();
}

/**
 * @see InfoInterface::name()
 */
QString LockPlugin::name() 
{
    return QString("Lock");
}

/**
 * @see InfoInterface::description()
 */
QString LockPlugin::description() 
{
    return QString("Lock or unlock the device");
}

/**
 * @see InfoInterface::pixmap()
 */
QPixmap * LockPlugin::pixmap()
{
    return pix;
}

/**
 * @see InfoInterface::aboutPlugin()
 */
void LockPlugin::aboutPlugin()
{
    QMessageBox::about(0, name(), "Lock or unlock the device, by ecksun (Linus Wallgren).");
}

/**
 * Execute the QDBusMessage on the system buss
 * There is no easy way of determening if the locking/unlocking was
 * successfull.
 */
bool LockPlugin::execute() 
{
    if (locking) {
        lock();
    }
    else {
        unlock();
    }
    message.setArguments(arguments);

    QDBusConnection::systemBus().call(message, QDBus::NoBlock);

    return true;
}

/**
 * @see ActionInterface::setConfig()
 */
void LockPlugin::setConfig(const QVariantMap & config) 
{
    if (config.contains("lock")) {
        locking = config["lock"].toBool();
    }
    else {
        locking = true;
    }
}

/**
 * @see InputInterface::configure()
 */
bool LockPlugin::configure()
{
    LockInputDialog lock;
    if (!lock.exec()) {
        return false;
    }
    locking = lock.isLocking();
    return true;
}

/**
 * @see InputInterface::configure(QString)
 * TODO
 */
bool LockPlugin::configure(QString)
{
    return false;
}

/**
 * @see InputInterface::config
 */
const QVariantMap & LockPlugin::config() 
{
    QVariantMap * map = new QVariantMap();
    map->insert("lock", locking);
    return *map;
}

Q_EXPORT_PLUGIN2(lock, LockPlugin);
