#include "ui_lock.h"
#include "lockInputDialog.h"

/**
 * Setup the UI and connect the button to the save slot
 */
LockInputDialog::LockInputDialog(QWidget *parent) : QDialog(parent)
{
    setupUi(this);

    connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
}

/**
 * Returns true if we should lock the device and false if we should unlock it
 * @return True if the device should be locked, false otherwise
 */
bool LockInputDialog::isLocking()
{
    return locking;
}

/**
 * Save the current state of the radio buttons and close the dialog
 */
void LockInputDialog::save() 
{
    if (lockRadioButton->isChecked()) {
        locking = true;
    }
    else {
        locking = false;
    }
    this->close();
}
