#include <QNetworkConfigurationManager>
#include <QDebug>
#include <iostream>
#include "wlan_listner.h"

/**
 * Connects the interesting signals to their corresponding slots
 */
WlanListner::WlanListner() : QObject() {
    /*
     * Connect the signals from QNetworkConfigurationManager to the slots of
     * this class
     */
    connect(&manager, SIGNAL(configurationAdded(const QNetworkConfiguration&)),
            this, SLOT(configurationAdded(const QNetworkConfiguration&)));

    connect(&manager, SIGNAL(configurationRemoved(const QNetworkConfiguration&)),
            this, SLOT(configurationRemoved(const QNetworkConfiguration&)));

    connect(&manager, SIGNAL(configurationChanged(const QNetworkConfiguration&)),
            this, SLOT(configurationChanged(const QNetworkConfiguration)));

    connect(&manager, SIGNAL(updateCompleted()), this, SLOT(updateCompleted()));

    connect(&manager, SIGNAL(onlineStateChanged(bool)), this ,SLOT(onlineStateChanged(bool)));

    /*
     * The code below is more or less debuging code, it lists all
     * configurations, however they seem to always have the same state, even if
     * we update them before geting them. This might be related to the fact
     * that the manager doesnt really know anything about the configurations
     * the results from the update gets in, more than that they are saved
     * (defined) that is. 
     */
    manager.updateConfigurations();
    QList<QtMobility::QNetworkConfiguration> configs = manager.allConfigurations();
    while (!configs.isEmpty()) {
        QtMobility::QNetworkConfiguration config = configs.takeFirst();
        if (config.isValid()) {
            qDebug() << config.name();
            qDebug() << config.state();
        }
        else {
            qDebug() << "Not Valid";
        }
    }
}

/**
 * Method called when a configuration is added
 */
void WlanListner::configurationAdded(const QNetworkConfiguration &config) {
    qDebug() << "WlanListner::configurationAdded()";
    print(config);
}

/**
 * The method called when a configuration is changed
 */
void WlanListner::configurationChanged(const QNetworkConfiguration &config) {
    qDebug() << "WlanListner::configurationChanged()";
    print(config);
}

/**
 * Method called when a configuration is removed
 */
void WlanListner::configurationRemoved(const QNetworkConfiguration &config) {
    qDebug() << "WlanListner::configurationRemoved()";
    print(config);
}

/**
 * Method called when the online state changes. Meaning the system either goes
 * online or offline
 */
void WlanListner::onlineStateChanged(bool isOnline) {
    qDebug() << "WlanListner::onlineStateChanged(" << isOnline << ")";
}

/**
 * Method called when the update is completed, for example after the
 * updateConfigurations() is called
 */
void WlanListner::updateCompleted() {
    qDebug() << "updateCompleted()";
}

/**
 * Helper method to print interensting information about a QNetworkConfiguration
 */
void WlanListner::print(const QNetworkConfiguration &config) {
    qDebug() << "name: " << config.name();
    qDebug() << "type: " << config.type();
    qDebug() << "state: " << config.state();
}
