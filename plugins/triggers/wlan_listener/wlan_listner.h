#ifndef WLAN_LISTNER_H
#define WLAN_LISTNER_H

#include <QObject>
#include <QNetworkConfigurationManager>

QTM_USE_NAMESPACE

class WlanListner : QObject {

    Q_OBJECT

    public:
        WlanListner();
    private slots:
        void    configurationAdded ( const QNetworkConfiguration & config );
        void    configurationChanged ( const QNetworkConfiguration & config );
        void    configurationRemoved ( const QNetworkConfiguration & configuration );
        void    onlineStateChanged ( bool isOnline );
        void    updateCompleted ();
    private:
        QNetworkConfigurationManager manager;
        void print(const QNetworkConfiguration &);
};

#endif
