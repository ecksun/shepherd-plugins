#ifndef SHEPHERD_POSITION_H
#define SHEPHERD_POSITION_H

#include <QObject>
#include <QNetworkConfigurationManager>
#include <QGeoPositionInfo>

QTM_USE_NAMESPACE

class Position : QObject {
    Q_OBJECT

    public:
        Position();
    private:
        void useSelfMonitor();
        void useAreaMonitor();
    private slots:
        void positionUpdated(const QGeoPositionInfo &);
        void areaEntered(const QGeoPositionInfo &);
        void areaExited(const QGeoPositionInfo &);
        
};

#endif
