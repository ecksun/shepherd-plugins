#include "position.h"
#include <QGeoPositionInfoSource>
#include <QGeoAreaMonitor>
#include <QDebug>

/**
 * Initiate the positioning service
 */
Position::Position() : QObject() {
    if (true) {
        useSelfMonitor();
    }
    else {
        useAreaMonitor();
    }
}

/**
 * Use changes in the position to find out where the position is
 */
void Position::useSelfMonitor() {
    QGeoPositionInfoSource *source = QGeoPositionInfoSource::createDefaultSource(0);

    if (source) {
        // Dont use GPS
        source->setPreferredPositioningMethods(QGeoPositionInfoSource::NonSatellitePositioningMethods);

        // Connect the signal to our slot
        connect(source, SIGNAL(positionUpdated(QGeoPositionInfo)),
                this, SLOT(positionUpdated(QGeoPositionInfo)));

        // Start the updates
        source->startUpdates();
    }
    else {
        qDebug() << "error! sadface";
    }

}

/*
 * Use an areaMonitor to look for changes in position
 *
 * NOTE:
 * I can't find a way to specify the method used when QGeoAreaMonitor gets the
 * position, I would like it to NOT use GPS to save power.
 * It might be so that QGeoAreaMonitor is smart and only uses the GPS when its
 * nescesary (the current position is near the border of the area). 
 *
 * TODO: find out how QGeoAreaMonitor works
 */
void Position::useAreaMonitor() {
    QGeoAreaMonitor *monitor = QGeoAreaMonitor::createDefaultMonitor(0);
    connect(monitor, SIGNAL(areaEntered(QGeoPositionInfo)),
            this, SLOT(areaEntered(QGeoPositionInfo)));
    connect(monitor, SIGNAL(areaExited(QGeoPositionInfo)),
            this, SLOT(areaExited(QGeoPositionInfo)));

    QGeoCoordinate bigBenLocation(51.50104, -0.124632);
    QGeoCoordinate bromsten(59.3750, 17.9250);
    monitor->setCenter(bigBenLocation);
    // monitor->setCenter(bromsten);
    monitor->setRadius(1000);
}

/**
 * Slot for when the position is updated
 *
 * @param info The QGeoPositionInfo object associated with the event
 */
void Position::positionUpdated(const QGeoPositionInfo &info) {
    qDebug() << "Position updated:" << info;
}

/**
 * Slot for when an area is entered
 *
 * @param info The QGeoPositionInfo object associated with the event
 */
void Position::areaEntered(const QGeoPositionInfo &info) {
    qDebug() << "Entered: " << info;
}

/**
 * Slot for when an area is exited
 *
 * @param info The QGeoPositionInfo object associated with the event
 */
void Position::areaExited(const QGeoPositionInfo &info) {
    qDebug() << "Exit: " << info;
}
