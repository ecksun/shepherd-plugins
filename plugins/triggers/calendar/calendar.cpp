#include "calendar.h"
#include <CMulticalendar.h>
#include <CEvent.h>
#include <QDBusConnection>
#include <vector>
#include <iostream>
#include <string>
#include <QString>
#include <QDebug>
#include <QDBusConnectionInterface>
#include <alarmd/libalarm.h>

using namespace Shepherd;

#define SHEPHERD_DBUS_SERVICE "org.shepherd.calendar"
#define SHEPHERD_DBUS_PATH "/shepherd/calendar"
// Make sure the _INTERFACE is in accordance with the header file
#define SHEPHERD_DBUS_INTERFACE "org.shepherd.calendar"

#define DEBUG 0

/**
 * Initialise the calendar
 */
Calendar::Calendar() : QObject() {
    if (DEBUG) std::cout << "Calendar constructor called" << std::endl;
    multiCalendar = CMulticalendar::MCInstance();

    // signal sender=:1.29 -> dest=(null destination) serial=40 path=/com/nokia/calendar; interface=com.nokia.calendar; member=dbChange
    //      string "CALENDAR-UI:1:EVENT:ADDED:1:6,"
    //      string "CALENDAR-UI"
    //       
    
    
    // Connect the signals from the calendar application to this class
    if (!QDBusConnection::sessionBus().connect("com.nokia.calendar", "/com/nokia/calendar", "com.nokia.calendar", "dbChange", this, SLOT(calendarChange(QString,QString)))) {
        qDebug() << "Could not connect to the calendar through dbus";
    }

    QDBusConnection bus = QDBusConnection::sessionBus();

    if (!bus.interface()->registerService(SHEPHERD_DBUS_SERVICE)) {
        qDebug() << "Could not register service: " << SHEPHERD_DBUS_SERVICE;
    }

    if (!bus.registerObject(SHEPHERD_DBUS_PATH, this, QDBusConnection::ExportAllSlots)) {
        qDebug() << "Could not register object: (" << SHEPHERD_DBUS_PATH << ", this, QDBusConnection::ExportAllSlots)";
    }

    if (DEBUG) qDebug() << "DBus connections complete, setting up alarm";
    
    setNextAlarm();
}

void Calendar::calendarChange(const QString arg1, const QString arg2) {
    qDebug() << "calendarChange!";
    qDebug() << arg1;
    qDebug() << arg2;
    setNextAlarm();
}

void Calendar::eventStart(QString id)  {
    qDebug() << "eventStart(" << id << ")";
    qDebug() << "event: " << getComponentFromId(id.toStdString())->toString().c_str();
    nextAlarm();
}

void Calendar::eventEnd(QString id) {
    qDebug() << "eventEnd(" << id << ")";
    qDebug() << "event: " << getComponentFromId(id.toStdString())->toString().c_str();
    nextAlarm();
}

void Calendar::nextAlarm() {
    qDebug() << "next alarm triggered!";

    setNextAlarm();
}

void Calendar::setNextAlarm() {
    clearAlarms();
    CEvent * nextEvent = getNextEvent();
    if (nextEvent) {
        qDebug() << "next event: " << nextEvent->getSummary().c_str();
        qDebug() << "\tid: " << nextEvent->getId().c_str();
        getComponentFromId(nextEvent->getId().c_str());
        // If the event has started
        if (nextEvent->getDateStart() < time(NULL)) {
            setAlarm(nextEvent->getDateEnd(), "eventEnd", nextEvent->getId());
        }
        else {
            setAlarm(nextEvent->getDateStart(), "eventStart", nextEvent->getId());
        }
    }
}

CComponent * Calendar::getComponentFromId(std::string id) {
    std::vector<string> ids;
    // I have no idea what this vector contains after the method call. On a
    // test run it contained one int, which was 1.
    std::vector<int> tmp;
    ids.push_back(id);

    int pErrorCode;
    std::vector<CComponent *> components = multiCalendar->getEventInList(ids,
            tmp, pErrorCode);

    if (DEBUG) qDebug() << "getEventInlist pErrorCode: " << pErrorCode;

    for (std::vector<CComponent *>::iterator component = components.begin();
            component != components.end(); ++component) {
        return *component;
    }
    return NULL;
}

void Calendar::setAlarm(int alarmTime, const char * methodName, std::string eventID) {
    alarm_event_t *newEvent = 0;

    alarm_action_t *act = 0;
    
    cookie_t resultCookie;
    
    newEvent = alarm_event_create();    //Create the default alarm struct.
    
    alarm_event_set_alarm_appid(newEvent, "ShepherdCalendarPlugin");
    
    alarm_event_set_title(newEvent, "nextCalendarEvent");

    newEvent->alarm_time = (time_t) alarmTime;
        
    act = alarm_event_add_actions(newEvent, 1);
    
    act->flags = ALARM_ACTION_WHEN_TRIGGERED | ALARM_ACTION_DBUS_USE_ACTIVATION | ALARM_ACTION_TYPE_DBUS;
    
    alarm_action_set_dbus_interface(act, SHEPHERD_DBUS_INTERFACE);
    alarm_action_set_dbus_service(act, SHEPHERD_DBUS_SERVICE);
    alarm_action_set_dbus_path(act, SHEPHERD_DBUS_PATH);
    alarm_action_set_dbus_name(act, methodName);

    qDebug() << "set_dbus_args";
    std::string arr[] = {eventID};
    // alarm_action_set_dbus_args wants an array..
    alarm_action_set_dbus_args(act, 's', arr, NULL);
    qDebug() << "set_dbus_args done";
        
    resultCookie = alarmd_event_add(newEvent);
    alarm_event_delete(newEvent);
    
    alarms.push_back(resultCookie);
}

void Calendar::clearAlarms() {
    for (std::vector<cookie_t>::iterator alarm = alarms.begin(); alarm !=
            alarms.end(); ++alarm) {
        int result = alarmd_event_del(*alarm);
        if (DEBUG) 
            qDebug() << "Deleting event, result: " << result;
    }
    alarms.clear();
}


/**
 * Returns the events currently happening that matches the specified arguments.
 *
 * @param summary The summary/name of the event to search for, if the string is
 * empty everything is matched
 * @param description The description of the event to find, if the parameter is
 * empty everything is matched 
 * @param location The position of the event, if it
 * is empty everything is matched
 *
 * @return A list of currently ongoing matching events
 */
std::vector<CEvent *> Calendar::getEvents(QString summary, QString description,
        QString location) {
    std::vector<CCalendar *> calendars = multiCalendar->getListCalFromMc();

    std::vector<CEvent *> returnList;

    for (std::vector<CCalendar *>::iterator it = calendars.begin(); it !=
            calendars.end(); ++it) {

        int err = 4711;
        std::vector< CEvent * > events = (*it)->getEvents(err);

        for (std::vector<CEvent *>::iterator event = events.begin();
                event != events.end(); ++event) {
            // Is the event currently happening?
            qDebug() << (*event)->getSummary().c_str() << ": " << (*event)->getDateStart() << " < " << time(NULL) << " && " << (*event)->getDateEnd() << " > " << time(NULL);
            if ((*event)->getDateStart() < time(NULL) && (*event)->getDateEnd() > time(NULL)) {
                QString sum((*event)->getSummary().c_str());
                QString desc((*event)->getDescription().c_str());
                QString loc((*event)->getLocation().c_str());

                if ( (summary.isEmpty()       || sum.toLower().contains(summary.toLower()) ) &&
                     (description.isEmpty()   || desc.toLower().contains(description.toLower())) &&
                     (location.isEmpty()      || loc.toLower().contains(location.toLower()) ) ) {

                    returnList.push_back(*event);
                }
            }
        }

        // Couldnt find any reference defining what the error codes actually
        // mean, but 500 seems to be good :)
        if (DEBUG) 
            std::cout << "pErrorCode: " << err << std::endl;
    }
    multiCalendar->releaseListCalendars(calendars);
    return returnList;
}

/**
 * Return all upcoming events.
 *
 * @return A list of future events
 */
std::vector<CEvent *> Calendar::getFutureEvents() {
    std::vector<CCalendar *> calendars = multiCalendar->getListCalFromMc();

    std::vector<CEvent *> returnList;

    for (std::vector<CCalendar *>::iterator it = calendars.begin(); it !=
            calendars.end(); ++it) {

        int err = 4711;
        std::vector< CEvent * > events = (*it)->getEvents(err);

        for (std::vector<CEvent *>::iterator event = events.begin();
                event != events.end(); ++event) {
            // Has the event happened yet?
            if ((*event)->getDateEnd() > time(NULL)) {
                returnList.push_back(*event);
            }
        }

        // Couldnt find any reference defining what the error codes actually
        // mean, but 500 seems to be good :)
        if (DEBUG) 
            std::cout << "pErrorCode: " << err << std::endl;
    }
    multiCalendar->releaseListCalendars(calendars);
    return returnList;
}

/**
 * Return the next upcoming event.
 *
 * @return The next event, NULL if there is no upcoming events
 */
CEvent * Calendar::getNextEvent() {
    std::vector<CCalendar *> calendars = multiCalendar->getListCalFromMc();

    // std::vector<CEvent *> returnList;
    int nextTime = 0;
    int anyEventChecked = false;
    CEvent * nextEvent = NULL;

    for (std::vector<CCalendar *>::iterator it = calendars.begin(); it !=
            calendars.end(); ++it) {
        
        if (DEBUG) qDebug() << "Looking at calendar: " << (*it)->toString().c_str();
        
        int err = 4711;
        std::vector< CEvent * > events = (*it)->getEvents(err);

        for (std::vector<CEvent *>::iterator event = events.begin();
                event != events.end(); ++event) {
            if(DEBUG) qDebug() << "Looking at event: " << (*event)->getSummary().c_str();
            // Has the event happened yet?
            if ((*event)->getDateEnd() > time(NULL)) {
                if (anyEventChecked == false || (*event)->getDateStart() < nextTime) {
                    anyEventChecked = true;
                    nextTime = (*event)->getDateStart();
                    nextEvent = *event;
                }
            }
        }

        // Couldnt find any reference defining what the error codes actually
        // mean, but 500 seems to be good :)
        if (DEBUG) 
            qDebug() << "pErrorCode: " << err;
    }
    if (DEBUG) qDebug() << "releasing calendars";
    multiCalendar->releaseListCalendars(calendars);
    if (DEBUG) {
        if (nextEvent == NULL) {
            qDebug() << "getNextEvent() returning NULL"; 
        }
        else {
            qDebug() << "getNextEvent() returning: " << nextEvent->getSummary().c_str();
        }
    }
    return nextEvent;
}

