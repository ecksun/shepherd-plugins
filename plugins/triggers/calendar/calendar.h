#ifndef SHEPHERD_CALENDAR_H
#define SHEPHERD_CALENDAR_H

#include <QObject>
#include <QDBusMessage>

#include <CMulticalendar.h>
#include <QString>
#include <CEvent.h>

#include <alarmd/libalarm.h>

#include <vector>

namespace Shepherd {
    class Calendar : QObject {
        Q_OBJECT
        
        // Perhaps this should be replaced by a XML file
        Q_CLASSINFO("D-Bus Interface", "org.shepherd.calendar")

        private:
            CMulticalendar * multiCalendar;
            void setNextAlarm();
            void setAlarm(int, const char *, std::string);
            std::vector<cookie_t> alarms;
            void clearAlarms();
        public:
            Calendar();

            std::vector<CEvent *> getEvents(QString, QString, QString);
            std::vector<CEvent *> getFutureEvents();
            CEvent * getNextEvent();
            CComponent * getComponentFromId(std::string);
            

        public slots:
            void calendarChange(const QString arg1, const QString arg2);
            void nextAlarm();
            void eventStart(QString);
            void eventEnd(QString);
    };
}

#endif
