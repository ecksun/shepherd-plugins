#ifndef SHEPHERD_TEST_CALENDAR_H
#define SHEPHERD_TEST_CALENDAR_H
#include <QtTest/QtTest>
#include <vector>
#include <CCalendar.h>

class TestCalendar : public QObject 
{
    Q_OBJECT
    private slots:
        void initTestCase();
        void cleanupTestCase();
        void testFindEvent();
    private:
        std::vector<std::string> eventIDs;
        CCalendar * calendar;

};
#endif
