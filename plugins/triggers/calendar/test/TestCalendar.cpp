#include "TestCalendar.h"
#include <iostream>
#include <CMulticalendar.h>
#include <CEvent.h>
#include "../calendar.h"


/**
 * Initiate the test case
 * Get the default calendar to do tests on and create events to search for
 */
void TestCalendar::initTestCase() {
    CMulticalendar * multiCalendar = CMulticalendar::MCInstance();
    if (! multiCalendar) {
        std::cerr << "no CMultiCalendar instance" << std::endl;
        return;
    }
    calendar = multiCalendar->getDefaultCalendar();
    if (! calendar) {
        std::cerr << "no default calendar" << std::endl;
        return;
    }

    // Create a new event spanning from one hour ago to in one hour
    CEvent event("testSummary", "testDescription", "test Location",
            time(NULL)-3600, time(NULL)+3600);

    int error = 4711;
    // I assume that if addEvent returns false something went wrong
    if (!calendar->addEvent(&event, error)) {
        std::cerr << "I think somethin went wrong, pErrorCode: " << error << std::endl;
    }

    eventIDs.push_back(event.getId());
    
}

/**
 * Try to find the events created when initiating
 */
void TestCalendar::testFindEvent() {
    Shepherd::Calendar cal;
    
    std::vector<CEvent *> events = cal.getEvents("", "", "");

    bool found = false;
    for (std::vector<CEvent *>::iterator event = events.begin();
            event != events.end(); ++event) {
        if ( (*event)->getSummary() == "testSummary" &&
                (*event)->getDescription() == "testDescription" &
                (*event)->getLocation() == "test Location" ) {
            found = true;
        }
        std::cout << (*event)->getSummary() << std::endl;
    }

    QVERIFY(found);
}

/**
 * Remove all new events from the calendar
 */
void TestCalendar::cleanupTestCase() {
    for (std::vector<std::string>::iterator event = eventIDs.begin();
            event != eventIDs.end(); ++event) {
        // CMulticalendar * multiCalendar = CMulticalendar::MCInstance();
        // // TODO fixa så den alltid använder samma kalender, dvs spara pekaren
        // CCalendar * calendar = multiCalendar->getDefaultCalendar();
        int error = 4711;
        calendar->deleteEvent(*event, error);
    }
}

QTEST_MAIN(TestCalendar)
