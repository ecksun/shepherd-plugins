######################################################################
# Automatically generated by qmake (2.01a) Tue Jun 1 23:42:45 2010
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

CONFIG += console
CONFIG += dbus

CONFIG += link_pkgconfig
PKGCONFIG += calendar-backend

QT += dbus
QT -= gui

TARGET = calendar_test

# Input
SOURCES += ../calendar.cpp
HEADERS += ../calendar.h

CONFIG += qtestlib
SOURCES -= main.cpp
SOURCES += TestCalendar.cpp
HEADERS += TestCalendar.h
QT += testlib
