#include "calendar.h"
#include <vector>
#include <CEvent.h>
#include <QCoreApplication>
#include <QDebug>

using namespace Shepherd;


int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Calendar cal;
    std::vector<CEvent *> events = cal.getFutureEvents();

    // for (std::vector<CEvent *>::iterator event = events.begin();
            // event != events.end(); ++event) {
        // std::cout << (*event)->getSummary() << ": " << (*event)->toString() << std::endl;
    // }
    qDebug() << "exec:";
    return app.exec();
    
}
