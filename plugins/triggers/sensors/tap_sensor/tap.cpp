#include "tap.h"
#include <QTapSensor>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * Im not sure this class works as I get no output. However it could be related
 * to the fact that the n900 doesnt have a tap sensor
 */
Tap::Tap(QObject* parent) : QObject(parent)
{
    m_sensor = new QTapSensor(this);
    m_sensor->setProperty("returnDoubleTapEvents", false);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Tap::~Tap() {
    delete m_sensor;
}
 
 
bool Tap::filter(QTapReading* reading)
{
    qDebug() << "doubletap: " << reading->isDoubleTap() << " - " << tapDirectionDescription(reading->tapDirection());
    return false;
}

/**
 * Get the description for a TapDirection.
 * The description are taken directly from the qtm API
 * @see http://doc.qt.nokia.com/qtmobility-1.0/qtapreading.html#TapDirection-enum
 * @param direction The direction of the tap
 * @return A description of the tap direction
 */
QString Tap::tapDirectionDescription(QTapReading::TapDirection direction)
{
    switch (direction)
    {
        case QTapReading::X:
            return "This flag is set if the tap was along the X axis.";
        case QTapReading::Y:
            return "This flag is set if the tap was along the Y axis.";
        case QTapReading::Z:
            return "This flag is set if the tap was along the Z axis.";
        case QTapReading::X_Pos:
            return "This value is returned if the tap was towards the positive X direction.";
        case QTapReading::Y_Pos:
            return "This value is returned if the tap was towards the positive Y direction.";
        case QTapReading::Z_Pos:
            return "This value is returned if the tap was towards the positive Z direction.";
        case QTapReading::X_Neg:
            return "This value is returned if the tap was towards the negative X direction.";
        case QTapReading::Y_Neg:
            return "This value is returned if the tap was towards the negative Y direction.";
        case QTapReading::Z_Neg:
            return "This value is returned if the tap was towards the negative Z direction.";
        default:
            return "Unknown direction";
    }
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Tap sensor;
    return app.exec();
}
