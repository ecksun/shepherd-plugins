#ifndef SHEPHERD_TAP_SENSOR_PLUGIN_HEADER
#define SHEPHERD_TAP_SENSOR_PLUGIN_HEADER

#include <QTapSensor>
#include <QTapFilter>

QTM_USE_NAMESPACE

class Tap : public QObject,
            public QTapFilter
{
    Q_OBJECT
    public:
        Tap(QObject * parent = 0);
        ~Tap();
    private slots:
        bool filter(QTapReading *);
    private:
        QTapSensor * m_sensor;
        QString tapDirectionDescription(QTapReading::TapDirection);
};

#endif
