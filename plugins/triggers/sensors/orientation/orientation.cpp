#include "orientation.h"
#include <QOrientationSensor>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
Orientation::Orientation(QObject* parent) : QObject(parent)
{
    m_sensor = new QOrientationSensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Orientation::~Orientation() {
    delete m_sensor;
}
 
 
bool Orientation::filter(QOrientationReading* reading)
{
    qDebug() << orientationDescription(reading->orientation());
    return false;
}

/**
 * Get the description for a Orientation.
 * The description are taken directly from the qtm API
 * @see http://doc.qt.nokia.com/qtmobility-1.0/qorientationreading.html#Orientation-enum
 * @param orientation The orientation
 * @return A description of the orientation
 */
QString Orientation::orientationDescription(QOrientationReading::Orientation orientation)
{
    switch (orientation)
    {
        case QOrientationReading::TopUp:
            return "The Top edge of the device is pointing up.";
        case QOrientationReading::TopDown:
            return "The Top edge of the device is pointing down.";
        case QOrientationReading::LeftUp:
            return "The Left edge of the device is pointing up.";
        case QOrientationReading::RightUp:
            return "The Right edge of the device is pointing up.";
        case QOrientationReading::FaceUp:
            return "The Face of the device is pointing up.";
        case QOrientationReading::FaceDown:
            return "The Face of the device is pointing down.";
        default:
            return "Unknown orientation";
    }
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Orientation sensor;
    return app.exec();
}
