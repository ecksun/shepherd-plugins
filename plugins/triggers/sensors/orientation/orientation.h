#ifndef SHEPHERD_ORIENTATION_PLUGIN_HEADER
#define SHEPHERD_ORIENTATION_PLUGIN_HEADER

#include <QOrientationSensor>
#include <QOrientationFilter>

QTM_USE_NAMESPACE

class Orientation : public QObject,
                    public QOrientationFilter
{
    Q_OBJECT
    public:
        Orientation(QObject * parent = 0);
        ~Orientation();
    private slots:
        bool filter(QOrientationReading *);
    private:
        QOrientationSensor * m_sensor;
        QString orientationDescription(QOrientationReading::Orientation);
};

#endif
