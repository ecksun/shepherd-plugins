#ifndef SHEPHERD_AMBIENT_LIGHT_SENSOR_PLUGIN_HEADER
#define SHEPHERD_AMBIENT_LIGHT_SENSOR_PLUGIN_HEADER

#include <QAmbientLightSensor>
#include <QAmbientLightFilter>

QTM_USE_NAMESPACE

class Light : public QObject,
                  public QAmbientLightFilter
{
    Q_OBJECT
    public:
        Light(QObject * parent = 0);
        ~Light();
    private slots:
        bool filter(QAmbientLightReading *);
    private:
        QAmbientLightSensor * m_sensor;
        QString lightDescription(QAmbientLightReading::LightLevel);
};

#endif
