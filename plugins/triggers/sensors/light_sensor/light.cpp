#include "light.h"
#include <QAmbientLightSensor>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
Light::Light(QObject* parent) : QObject(parent)
{
    m_sensor = new QAmbientLightSensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Light::~Light() {
    delete m_sensor;
}
 
 
bool Light::filter(QAmbientLightReading* reading)
{
    qDebug() << lightDescription(reading->lightLevel());
    return false;
}

/**
 * Get the description for a LightLevel.
 * The description are taken directly from the qtm API
 * @see http://doc.qt.nokia.com/qtmobility-1.0/qambientlightreading.html#LightLevel-enum
 * @param level The LightLevel to which a description should be returned
 * @return A description of the lightlevel
 */
QString Light::lightDescription(QAmbientLightReading::LightLevel level)
{
    switch (level)
    {
        case QAmbientLightReading::Dark: 
            return "It is dark.";
        case QAmbientLightReading::Twilight:
            return "It is moderately dark.";
        case QAmbientLightReading::Light:
            return "It is light (eg. internal lights).";
        case QAmbientLightReading::Bright:
            return "It is bright (eg. shade).";
        case QAmbientLightReading::Sunny:
            return "It is very bright (eg. direct sunlight).";
        default:
            return "Unknown level";
    }
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Light sensor;
    return app.exec();
}
