#include "accelerometer.h"
#include <QAccelerometer>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
AccelerationInfo::AccelerationInfo(QObject* parent) : QObject(parent)
{
    m_sensor = new QAccelerometer(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

AccelerationInfo::~AccelerationInfo() {
    delete m_sensor;
}
 
 
    // Override of QAcclerometerFilter::filter(QAccelerometerReading*)
bool AccelerationInfo::filter(QAccelerometerReading* reading)
{
    qreal x = reading->x();
    qreal y = reading->y();
    qreal z = reading->z();

    // Process acceleration sensor readings ...

    qDebug("Current device acceleration: x=%f y=%f z=%f", x, y, z);

    return false;
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    AccelerationInfo sensor;
    return app.exec();
}
