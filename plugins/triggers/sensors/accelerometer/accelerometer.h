#ifndef SHEPHERD_ACCELEROMETER_PLUGIN_HEADER
#define SHEPHERD_ACCELEROMETER_PLUGIN_HEADER

#include <QAccelerometer>
#include <QAccelerometerFilter>

QTM_USE_NAMESPACE

class AccelerationInfo : public QObject,
                         public QAccelerometerFilter
{
    Q_OBJECT
    public:
        AccelerationInfo(QObject * parent = 0);
        ~AccelerationInfo();
    private slots:
        bool filter(QAccelerometerReading *);
    private:
        QAccelerometer * m_sensor;
};

#endif
