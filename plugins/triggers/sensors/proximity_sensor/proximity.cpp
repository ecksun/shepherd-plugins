#include "proximity.h"
#include <QProximitySensor>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
Proximity::Proximity(QObject* parent) : QObject(parent)
{
    m_sensor = new QProximitySensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Proximity::~Proximity() {
    delete m_sensor;
}
 
 
bool Proximity::filter(QProximityReading* reading)
{
    qDebug() << "something is close: " << reading->close();
    return false;
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Proximity sensor;
    return app.exec();
}
