#ifndef SHEPHERD_PROXIMITY_SENSOR_PLUGIN_HEADER
#define SHEPHERD_PROXIMITY_SENSOR_PLUGIN_HEADER

#include <QProximitySensor>
#include <QProximityFilter>

QTM_USE_NAMESPACE

class Proximity : public QObject,
                  public QProximityFilter
{
    Q_OBJECT
    public:
        Proximity(QObject * parent = 0);
        ~Proximity();
    private slots:
        bool filter(QProximityReading *);
    private:
        QProximitySensor * m_sensor;
};

#endif
