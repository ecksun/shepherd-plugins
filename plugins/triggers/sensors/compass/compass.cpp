#include "compass.h"
#include <QCompass>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * This class is untested as there is no output on the N900 (probably because
 * there is no compass in that device
 */
Compass::Compass(QObject* parent) : QObject(parent)
{
    m_sensor = new QCompass(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Compass::~Compass() {
    delete m_sensor;
}
 
 
bool Compass::filter(QCompassReading* reading)
{
    qDebug() << "We are heading: " << reading->azimuth() << " with calibration level " << reading->calibrationLevel();
    return false;
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Compass sensor;
    return app.exec();
}
