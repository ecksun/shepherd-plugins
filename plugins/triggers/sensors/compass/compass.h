#ifndef SHEPHERD_COMPASS_PLUGIN_HEADER
#define SHEPHERD_COMPASS_PLUGIN_HEADER

#include <QCompass>
#include <QCompassFilter>

QTM_USE_NAMESPACE

class Compass : public QObject,
              public QCompassFilter
{
    Q_OBJECT
    public:
        Compass(QObject * parent = 0);
        ~Compass();
    private slots:
        bool filter(QCompassReading *);
    private:
        QCompass * m_sensor;
};

#endif
