#ifndef SHEPHERD_COMPASS_PLUGIN_HEADER
#define SHEPHERD_COMPASS_PLUGIN_HEADER

#include <QMagnetometer>
#include <QMagnetometerFilter>

QTM_USE_NAMESPACE

class Magnetometer : public QObject,
              public QMagnetometerFilter
{
    Q_OBJECT
    public:
        Magnetometer(QObject * parent = 0);
        ~Magnetometer();
    private slots:
        bool filter(QMagnetometerReading *);
    private:
        QMagnetometer * m_sensor;
};

#endif
