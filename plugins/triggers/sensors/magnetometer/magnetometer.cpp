#include "magnetometer.h"
#include <QMagnetometer>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
/**
 * This class is untested as there is no output on the N900 (probably because
 * there is no magnetometer in that device)
 */
Magnetometer::Magnetometer(QObject* parent) : QObject(parent)
{
    m_sensor = new QMagnetometer(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Magnetometer::~Magnetometer() {
    delete m_sensor;
}
 
 
bool Magnetometer::filter(QMagnetometerReading* reading)
{
    qDebug("Magnetometer (x,y,z): (%f, %f, %f)", reading->x(), reading->y(), reading->z());
    return false;
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Magnetometer sensor;
    return app.exec();
}
