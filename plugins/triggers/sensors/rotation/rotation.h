#ifndef SHEPHERD_ROTATION_SENSOR_PLUGIN_HEADER
#define SHEPHERD_ROTATION_SENSOR_PLUGIN_HEADER

#include <QRotationSensor>
#include <QRotationFilter>

QTM_USE_NAMESPACE

class Rotation : public QObject,
                    public QRotationFilter
{
    Q_OBJECT
    public:
        Rotation(QObject * parent = 0);
        ~Rotation();
    private slots:
        bool filter(QRotationReading *);
    private:
        QRotationSensor * m_sensor;
};

#endif
