#include "rotation.h"
#include <QRotationSensor>
#include <QDebug>
 
// Neccessary for Qt Mobility API usage
QTM_USE_NAMESPACE
 
 
Rotation::Rotation(QObject* parent) : QObject(parent)
{
    m_sensor = new QRotationSensor(this);
    m_sensor->addFilter(this);
    m_sensor->start();
}

Rotation::~Rotation() {
    delete m_sensor;
}
 
 
bool Rotation::filter(QRotationReading* reading)
{
    qDebug("rotation (x,y,z): (%f, %f, %f)", reading->x(), reading->y(), reading->z());
    return false;
}
 
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    Rotation sensor;
    return app.exec();
}
