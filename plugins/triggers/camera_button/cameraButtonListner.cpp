#include "cameraButtonListner.h"
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDebug>

#define SHEPHERD_SHUTTER_SERVICE "org.freedesktop.Hal"
#define SHEPHERD_SHUTTER_FOCUS_PATH "/org/freedesktop/Hal/devices/platform_cam_focus"
#define SHEPHERD_SHUTTER_SHUTTER_PATH "/org/freedesktop/Hal/devices/platform_cam_launch"
#define SHEPHERD_SHUTTER_INTERFACE "org.freedesktop.Hal.Device"

/**
 * Connect the dbus signals to our methods for handling them
 */
CameraButtonListner::CameraButtonListner()
{
    QDBusConnection::systemBus().connect(
            SHEPHERD_SHUTTER_SERVICE,
            SHEPHERD_SHUTTER_FOCUS_PATH,
            SHEPHERD_SHUTTER_INTERFACE,
            "Condition",
            this,
            SLOT(focusPressed(QString, QString))
            );
    
    QDBusConnection::systemBus().connect(
            SHEPHERD_SHUTTER_SERVICE,
            SHEPHERD_SHUTTER_SHUTTER_PATH,
            SHEPHERD_SHUTTER_INTERFACE,
            "Condition",
            this,
            SLOT(shutterPressed(QString, QString))
            );

}

/**
 * Recive the focus pressed signal
 * I havent been able to figure out the real purpouse of the arguments and have
 * no real use for them, so they are ignored
 */
void CameraButtonListner::focusPressed(QString, QString)
{
    QDBusInterface propertyInterface(SHEPHERD_SHUTTER_SERVICE,
            SHEPHERD_SHUTTER_FOCUS_PATH,
            SHEPHERD_SHUTTER_INTERFACE,
            QDBusConnection::systemBus());
    
    bool pressed = propertyInterface.call("GetProperty", "button.state.value").arguments().at(0).toBool();

    if (pressed)
        qDebug() << "Focus is pressed";
    else
        qDebug() << "Focus is released";
}

/**
 * Recive the shutter pressed signal (the signal is emited when the button is
 * pressed all the way down)
 * I havent been able to figure out the real purpouse of the arguments and have
 * no real use for them, so they are ignored
 */
void CameraButtonListner::shutterPressed(QString, QString)
{
    QDBusInterface propertyInterface(SHEPHERD_SHUTTER_SERVICE,
            SHEPHERD_SHUTTER_SHUTTER_PATH,
            SHEPHERD_SHUTTER_INTERFACE,
            QDBusConnection::systemBus());
    
    bool pressed = propertyInterface.call("GetProperty", "button.state.value").arguments().at(0).toBool();

    if (pressed)
        qDebug() << "Shutter is pressed";
    else
        qDebug() << "Shutter is released";
}

#undef SHEPHERD_SHUTTER_SERVICE
#undef SHEPHERD_SHUTTER_FOCUS_PATH
#undef SHEPHERD_SHUTTER_SHUTTER_PATH
#undef SHEPHERD_SHUTTER_INTERFACE
