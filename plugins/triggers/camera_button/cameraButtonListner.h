#ifndef SHEPHERD_CAMERA_BUTTON_PLUGIN_HEADER
#define SHEPHERD_CAMERA_BUTTON_PLUGIN_HEADER
#include <QCoreApplication>

class CameraButtonListner : public QObject
{
    Q_OBJECT
    public:
        CameraButtonListner();

    public slots:
        void focusPressed(QString, QString);
        void shutterPressed(QString, QString);

};

#endif
