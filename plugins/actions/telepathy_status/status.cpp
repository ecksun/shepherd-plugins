#include "status.h"
#include <QObject>
#include <TelepathyQt4/Types>
#include <TelepathyQt4/Account>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/PendingOperation>
#include <TelepathyQt4/PendingReady>

#include <TelepathyQt4/Constants>


#include <QDebug>

/*
 * This code is probably only compatible with Telepathy-Qt4 0.2.x because
 * AccountManagetr->onlineAccounts doesnt exist, likewise with AccountSet
 */

Status::Status() : QObject() 
{
    accountManager = Tp::AccountManager::create(); 

    connect(accountManager->becomeReady(),
            SIGNAL(finished(Tp::PendingOperation*)),
            SLOT(accountManagerReady(Tp::PendingOperation*)));
}

void Status::accountManagerReady(Tp::PendingOperation * op)
{
    if (op->isError()) {
        qDebug() << "Account manager cannot become ready:" <<
            op->errorName() << "-" << op->errorMessage();
        return;
    }

    setStatus();

}

/**
 * Get the accounts to change status for
 * 
 * NOTE this method requires the AccountManager to be ready
 */
void Status::setStatus()
{
    QList<Tp::AccountPtr> accounts = accountManager->validAccounts();

    qDebug() << accounts.size();

    Tp::SimplePresence presence;

    presence.type = 1;
    presence.status = "brb";
    presence.statusMessage = "wopdido";

    for (int i = 0; i < accounts.size(); ++i) {
        qDebug() << accounts[i]->displayName();
        accounts[i]->setRequestedPresence(presence);
    }
}
