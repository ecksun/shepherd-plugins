#ifndef SHEPHERD_STATUS_H
#define SHEPHERD_STATUS_H

#include <QObject>
#include <QStringList>
#include <TelepathyQt4/Types>
#include <TelepathyQt4/AccountManager>

namespace Tp {
    class PendingOperation;
}


class Status : QObject {

    Q_OBJECT

    public:
        Status();

    private Q_SLOTS:
        void accountManagerReady(Tp::PendingOperation *);

    private:
        void changeStatus(QString);
        Tp::AccountManagerPtr accountManager;
        void setStatus();
};

#endif
