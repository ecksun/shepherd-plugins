#include "status.h"
#include <QCoreApplication>

int main(int argc, char * argv[]) {
    // QCoreApplication are needed for argument handling
    QCoreApplication app(argc, argv);
    Status status;
    // We dont need a main loop
    return app.exec();
}
