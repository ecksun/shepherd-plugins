#include "cellular.h"
#include <QDBusConnection>
#include <QDBusMessage>
#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include <QDBusArgument>

Cellular::Cellular() : QObject() {
    QStringList argv = QCoreApplication::arguments();

    for (QStringList::iterator it = argv.begin(); it != argv.end(); ++it) {
        qDebug() << *it;       
    }
    QList<QVariant> radioArguments;
    QList<QVariant> radioModeArguments;
    if (argv.size() > 1) {
        if (argv[1].compare("off") == 0) {
            radioArguments.append(false);
        }
        else if (argv[1].compare("2G") == 0) {
            radioModeArguments.append((unsigned char)1);
            qDebug() << radioModeArguments[0].type();
        }
        else if (argv[1].compare("3G") == 0) {
            radioModeArguments.append(2);
        }
    }

    if (radioArguments.size() == 0 && radioModeArguments.size() == 0) {
        // Reset to "normal"
        radioArguments.append(true);
        radioModeArguments.append(0);
    }
    
    if (radioArguments.size() > 0) {
        QDBusMessage message = QDBusMessage::createMethodCall("com.nokia.phone.SSC", "/com/nokia/phone/SSC", "com.nokia.phone.SSC", "set_radio");
        message.setArguments(radioArguments);

        QDBusConnection::systemBus().call(message, QDBus::NoBlock);
    }

    if (radioModeArguments.size() > 0) {
        QDBusMessage message = QDBusMessage::createMethodCall("com.nokia.phone.net", "/com/nokia/phone/net", "Phone.Net", "set_selected_radio_access_technology");
        message.setArguments(radioModeArguments);

        QDBusMessage result = QDBusConnection::systemBus().call(message, QDBus::Block);
        qDebug() << "Error:";
        qDebug() << result.errorName();
        qDebug() << result.errorMessage();

        QList<QVariant> resultArguments = result.arguments();

        for (int i = 0; i < resultArguments.size(); ++i) {
            qDebug() << resultArguments[i];
        }
    }
}
