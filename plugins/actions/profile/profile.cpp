#include <libprofile.h>
#include <iostream>
#include <string>

#define DEBUG true

// Depends on libprofile-dev
int main(int argc, char * argv[]) {
    std::string newProfile;
    if (argc > 1) {
        newProfile = argv[1];
    }
    else {
        char ** profiles  = profile_get_profiles();

        // Find the next profile after the current
        int profileIndex = -1;
        int i;
        for (i = 0; profiles[i] != 0; ++i) {
            if (strcmp(profiles[i], profile_get_profile()) == 0) { 
                // Do I need to free the memory the pointer of
                // profile_get_profile() is pointing to?
                profileIndex = i;
            }
        }

        newProfile  = profiles[ (profileIndex+1) % i ];

        profile_free_profiles(profiles);
    }
    if (profile_set_profile(newProfile.c_str()) != 0 && DEBUG) {
        std::cerr << "profile_set_profile() gave an error " << std::endl;
    }
}
