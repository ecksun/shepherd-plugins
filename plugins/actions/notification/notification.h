#ifndef SHEPHERD_NOTIFICATION_H
#define SHEPHERD_NOTIFICATION_H

#include <QObject>
#include <string>

namespace Shepherd {
    class Notification : QObject {
        Q_OBJECT

        public:
            static void notify(std::string);
    };
}

#endif
