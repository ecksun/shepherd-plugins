/*
 * Apperently #include<libnotify/notify.h> needs to be defined first because of
 * some #includes conflicts between Qt and libnotify
 *
 * See
 * http://discussion.forum.nokia.com/forum/showthread.php?190686-how-can-i-display-notification-on-N900-screen
 * for more information
 */
#include <libnotify/notify.h>

#include <notification.h>
#include <QDebug>

using namespace Shepherd;

/**
 * Display a notification
 *
 * @argument message The message to be displayed in the notification
 */
void Notification::notify(std::string message) {
    NotifyNotification *notification;

    // Init libnotify library
    notify_init("shepherd");

    // Create notification
    notification = notify_notification_new("shepherd", message.c_str(), NULL, NULL);

    if (notification) {
        // Set timeout
        notify_notification_set_timeout(notification, 3000);

        // Schedule notification for showing
        if (!notify_notification_show(notification, NULL))
            qDebug("Failed to send notification");

        // Clean up the memory
        g_object_unref(notification);
    } 
    else {
        qDebug("Failed to create notification");
    }
}
