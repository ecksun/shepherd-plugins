#ifndef LOCK_H
#define LOCk_H

#include <QObject>
#include <QDBusMessage>


class Lock : QObject {

    Q_OBJECT

    public:
        Lock();
        void lock();
        void unlock();

    private:
        void execute();
        QList<QVariant> arguments;
        QDBusMessage message;
};

#endif
