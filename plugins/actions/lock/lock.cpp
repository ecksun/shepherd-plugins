#include "lock.h"
#include <QDBusConnection>
#include <QDBusMessage>
#include <QCoreApplication>
#include <QStringList>

/**
 * Handle and initiate the right arguments for the DBus message
 * 
 * Initiate the arguments needed for the DBus message that are going to be used
 * to lock and unlock the device The constructor does also take care of the
 * argument handling.
 */
Lock::Lock() : QObject() {
    QStringList argv = QCoreApplication::arguments();

    // the following arguments are the same independent of locking or unlocking
    // the device
    arguments.append("com.nokia.mce");
    arguments.append("/com/nokia/mce/request");
    arguments.append("com.nokia.mce.request");
    arguments.append("devlock_callback");
    
    if (argv.size() > 1 && argv[1].compare("off") == 0) {
        unlock();
    }
    else {
        lock();
    }

}

/**
 * Prepare the QDBusMessage for locking the device
 *
 * NOTE: There are going to be problems if this or the unlock method are run
 * several times, as the argument list is only extended with the assumption
 * that only the constructor has been run previously
 */
void Lock::lock() {
    message = QDBusMessage::createMethodCall("com.nokia.system_ui",
            "/com/nokia/system_ui/request", "com.nokia.system_ui.request",
            "devlock_open");
    
    // The 5th argument
    arguments.append((unsigned int)3);
    execute();
}

/**
 * Prepare the QDBusMessage for unlocking the deivce
 *
 * NOTE: There are going to be problems if this or the unlock method are run
 * several times, as the argument list is only extended with the assumption
 * that only the constructor has been run previously
 */
void Lock::unlock() {
    message = QDBusMessage::createMethodCall("com.nokia.system_ui",
            "/com/nokia/system_ui/request", "com.nokia.system_ui.request",
            "devlock_close");
    // The 5th argument
    arguments.append((unsigned int)0);
    execute();
}

/**
 * Execute the QDBusMessage on the system buss
 */
void Lock::execute() {
    message.setArguments(arguments);

    // We are not interested in the output, only usefull for debuging purpouses
    QDBusConnection::systemBus().call(message, QDBus::NoBlock);
}
